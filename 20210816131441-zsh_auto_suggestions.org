:PROPERTIES:
:ID:       41efcea6-fd9a-45e1-89d5-751fdd3f92dd
:ROAM_REFS: https://github.com/zsh-users/zsh-autosuggestions
:END:
#+title: zsh-auto-suggestions
#+filetags: zsh pluging sw

Fish-like fast/unobtrusive autosuggestions for [[id:159d5a2f-f76b-45e7-9028-b67f776cda07][zsh]]. It suggests commands as you type based on
history and completions.

https://github.com/zsh-users/zsh-autosuggestions
