:PROPERTIES:
:ID:       79407992-f984-4425-871b-851ccca924f6
:header-args: :mkdirp yes :tangle-mode (identity #o444)
:END:
#+filetags: :emacs:miscellaneous:
#+title: emacs miscellaneous

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#header][Header]]
- [[#initial-minibuffer][Initial Minibuffer]]
- [[#unfill-paragraph][Unfill paragraph]]
- [[#smart-line-numbers][Smart Line Numbers]]
- [[#after-jump][After jump]]
- [[#create-new-empty-buffer][Create new Empty Buffer]]
- [[#highlight-annotations][Highlight Annotations]]
- [[#kill-symbol-at-point][Kill symbol at point]]
- [[#highlight-comments][Highlight Comments]]
- [[#re-open-read-only-files-as-root-automagically][Re-open read-only files as root automagically]]
- [[#repeatable-commands][Repeatable commands]]
- [[#secure-box][Secure box]]
- [[#instant-access-to-files][Instant access to files]]
- [[#beautify-round-quotes][Beautify round quotes]]
- [[#fix-apostrophes][Fix apostrophes]]
- [[#check-online][Check online]]
- [[#faster-pop-to-mark-command][Faster pop-to-mark command]]
- [[#trailer][Trailer]]
- [[#old-code][Old code]]
  - [[#simple-menu][Simple Menu]]
  - [[#remove--completes-lists][Remove / Completes lists]]
  - [[#eval-result-overlays-in-emacs-lisp][Eval-result-overlays in Emacs-lisp]]

* Introduction

* Header

#+begin_src emacs-lisp
;; package --- Summary
;;
;;; Commentary:
;;
;; This file contains defaults for variables as well as global keystrokes
;;; Code:
#+end_src

* Initial Minibuffer

Based on [[https://stackoverflow.com/questions/20891095/programatically-insert-text-into-command-that-normally-blocks-event-loop][this]] answer.

#+begin_src emacs-lisp
  (defun with-initial-minibuffer (str)
    (interactive)
    (funcall `(lambda ()
                (interactive)
                (minibuffer-with-setup-hook
                    (lambda () (insert (format "%s " ,str)))
                  (call-interactively 'helm-M-x)))))
#+end_src

* Unfill paragraph

Found [[https://www.emacswiki.org/emacs/UnfillParagraph][here]].

#+BEGIN_SRC emacs-lisp
  (defun unfill-paragraph (&optional region)
    "Takes a multi-line paragraph and makes it into a single line of text."
    (interactive (progn (barf-if-buffer-read-only) '(t)))
    (let ((fill-column (point-max))
          ;; This would override `fill-column' if it's an integer.
          (emacs-lisp-docstring-fill-column t))
      (fill-paragraph nil region)))
#+END_SRC

* After jump

To be used in after advices for customising functions behaviour (smartscan, dumb-jump, outshine,
helm actions, etc.). Just checks if a major mode related function exists and calls it.

#+begin_src emacs-lisp
  (defun csb/generic-after-jump-advice (&optional arg)
    (let ((titi (intern (format "csb/generic-after-jump-advice-%s"
                                (car (split-string
                                      (symbol-name major-mode) "-mode"))))))
      (when (fboundp titi)
        (funcall titi arg))))
#+end_src

* Create new Empty Buffer

#+begin_src emacs-lisp
  (defun csb/new-empty-buffer ()
    "Open a new empty buffer."
    (interactive)
    (let ((buf (generate-new-buffer "untitled")))
      (switch-to-buffer buf)
      (funcall (and 'emacs-lisp-mode))
      (setq buffer-offer-save t)))
  (global-set-key (kbd "C-x C-n") 'csb/new-empty-buffer)
#+end_src

* Compdef

#+begin_src emacs-lisp :tangle no
  (use-package compdef
    ;; :pin melpa)
#+end_src

* Hide lines

#+begin_src emacs-lisp :tangle no
  (use-package hide-lines
    :config
    (defvar-local csb/hide-lines-regexp nil
      "regexp to use with hide-lines")
    (defun csb/hide-lines (&optional arg)
      "Handle hide lines comments."
      (interactive "P")
      (when csb/hide-lines-regexp
        (cond ((equal arg '(4))
               ;; (message "show all lines")
               (hide-lines-show-all))
              ((equal arg '(16))
               ;; (message (format "hide all but regexp %s" csb/hide-lines-regexp))
               (hide-lines-not-matching csb/hide-lines-regexp))
              (t
               ;; (message (format "hide regexp %s" csb/hide-lines-regexp))
               (hide-lines-matching csb/hide-lines-regexp)))))
    :bind
    (:map prog-mode-map
          (("C-c /" . csb/hide-lines))))
#+end_src

* Highlight Annotations

Based on [[http://emacsredux.com/blog/2013/07/24/highlight-comment-annotations/][Emacs Redux article]]

I use =hl-todo= instead.

* Kill symbol at point

#+begin_src emacs-lisp
  (defun csb/kill-symbol-at-point()
    (interactive)
    (setq current-point (point))
    (setq bds (bounds-of-thing-at-point 'symbol))
    (setq p1 (car bds) )
    (setq p2 (cdr bds) )
    (copy-region-as-kill p1 p2)
    ;; (setq csb/kill-symbol-at-point-thing (buffer-substring-no-properties p1 p2))
    (goto-char current-point))
  (global-set-key (kbd "C-x M-k") 'csb/kill-symbol-at-point)
#+end_src

* Highlight Comments

Open file in external program
http://batsov.com/articles/2011/11/12/emacs-tip-number-2-open-file-in-external-program/

* Re-open read-only files as root automagically

https://tsdh.wordpress.com/2008/08/20/re-open-read-only-files-as-root-automagically/

#+begin_src emacs-lisp :tangle no
  (defun th-rename-tramp-buffer ()
    (when (file-remote-p (buffer-file-name))
      (rename-buffer
       (format "%s:%s"
               (file-remote-p (buffer-file-name) 'method)
               (buffer-name)))))

  (add-hook 'find-file-hook
            'th-rename-tramp-buffer)

  (defadvice find-file (around th-find-file activate)
    "Open FILENAME using tramp's sudo method if it's read-only."
    (if (and (not (file-writable-p (ad-get-arg 0)))
             (y-or-n-p (concat "File "
                               (ad-get-arg 0)
                               " is read-only.  Open it as root? ")))
        (th-find-file-sudo (ad-get-arg 0))
      ad-do-it))

  (defun th-find-file-sudo (file)
    "Opens FILE with root privileges."
    (interactive "F")
    (set-buffer (find-file (concat "/sudo::" file))))
#+end_src

* Repeatable commands

#+begin_src emacs-lisp
  (defun make-repeatable-command (cmd)
    "Returns a new command that is a repeatable version of CMD.
  The new command is named CMD-repeat.  CMD should be a quoted
  command.

  This allows you to bind the command to a compound keystroke and
  repeat it with just the final key.  For example:

    (global-set-key (kbd \"C-c a\") (make-repeatable-command 'foo))

  will create a new command called foo-repeat.  Typing C-c a will
  just invoke foo.  Typing C-c a a a will invoke foo three times,
  and so on."
    (fset (intern (concat (symbol-name cmd) "-repeat"))
          `(lambda ,(help-function-arglist cmd) ;; arg list
             ,(format "A repeatable version of `%s'." (symbol-name cmd)) ;; doc string
             ,(interactive-form cmd) ;; interactive form
             ;; see also repeat-message-function
             (setq last-repeatable-command ',cmd)
             (repeat nil)))
    (intern (concat (symbol-name cmd) "-repeat")))
#+end_src

http://zck.me/emacs-repeat-emacs-repeat
http://irreal.org/blog/?p=5543

* Secure box

#+begin_src emacs-lisp
  (defun csb/sb-check-open ()
    "veriry secure box is open"
    (and (zerop (shell-command "mountpoint -q /home/csantos/sb"))
         (file-exists-p "~/sb/.password-store/")
         (file-exists-p "~/.gnupg/pubring.gpg")))
#+end_src

* Instant access to files

#+begin_src emacs-lisp
  (defun find-user-init-file ()
    "Show `user-init-file' in its own tab."
    (interactive)
    (find-file (expand-file-name
                (format "%s/emacs.org" csb/perso-wiki-path))))

  (global-set-key (kbd "<f12>") #'find-user-init-file)
#+end_src

#+begin_src emacs-lisp
  (defun csb/find-user-todo-file ()
    "Show `user-init-file' in its own tab."
    (interactive)
    (unless (get-buffer "*Telega Root*")
      (call-interactively #'csb/telega)
      (quit-window))
    (sleep-for 1)
    (org-link-open-from-string
     "[[telega:-1001166929514?open_content#1289][Notes]]"))

  (global-set-key (kbd "<f9>") #'csb/find-user-todo-file)
#+end_src

TODO: replace by defmacro

#+begin_src emacs-lisp :tangle no
  (global-set-key (kbd "<f3>")
                  #'(lambda ()
                      (interactive)
                      (cond ((string= major-mode 'cfw:calendar-mode)
                             (find-file (concat org-config-path
                                                "csb-calendar.page")))
                            ((string= major-mode 'vhdl-mode)
                             (find-file (concat org-config-path
                                                "csb-vhdl.page")))
                            ((string= major-mode 'org-mode)
                             (find-file (concat org-config-path
                                                "csb-orgmode.page")))
                            ((string= major-mode 'matlab-mode)
                             (find-file (concat org-config-path
                                                "csb-matlab.page")))
                            ((string= major-mode 'eww-mode)
                             (find-file (concat org-config-path
                                                "csb-www.page")))
                            ((string= major-mode 'python-mode)
                             (find-file (concat org-config-path
                                                "csb-python.page")))
                            ((string-prefix-p "mu4e" (symbol-name major-mode))
                             (find-file (concat org-config-path
                                                "csb-mail.page")))
                            ((string= major-mode 'elfeed-search-mode)
                             (find-file (concat org-config-path
                                                "csb-elfeed.page")))
                            ((string= major-mode 'matlab-mode)
                             (find-file (concat org-config-path
                                                "csb-matlab.page")))
                            ((derived-mode-p 'c-mode 'c++-mode)
                             (find-file (concat org-config-path
                                                "csb-c-mode.page")))
                            (t nil))
                      (csb/toggle-fringes)))

  (global-set-key (kbd "<f4>")
                  #'(lambda ()
                      (interactive)
                      (cond ((string= major-mode 'cfw:calendar-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-calendar.page")))
                            ((string= major-mode 'org-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-orgmode.page")))
                            ((string= major-mode 'eww-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-www.page")))
                            ((string= major-mode 'vhdl-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-vhdl.page")))
                            ((string= major-mode 'matlab-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-matlab.page")))
                            ((string= major-mode 'eww-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-www.page")))
                            ((string= major-mode 'python-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-python.page")))
                            ((string-prefix-p "mu4e" (symbol-name major-mode))
                             (find-file-other-window (concat org-config-path
                                                             "csb-mail.page")))
                            ((string= major-mode 'elfeed-search-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-elfeed.page")))
                            ((string= major-mode 'matlab-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-matlab.page")))
                            ((derived-mode-p 'c-mode 'c++-mode)
                             (find-file-other-window (concat org-config-path
                                                             "csb-c-mode.page")))
                            (t nil))
                      (csb/toggle-fringes)))

  ;; (defmacro set-config-file (filename)
  ;;   (local-set-key (kbd "<f3>")
  ;;                  `(lambda()
  ;;                     (interactive)
  ;;                     (find-file-other-window (concat org-config-path ,filename))))
  ;;   (local-set-key (kbd "<f4>")
  ;;                  `(lambda()
  ;;                     (interactive)
  ;;                     (find-file (concat org-config-path ,filename)))))

  ;; (defmacro find-config-file-other-window (filename)
  ;;   "Edit the elfeed-mode config file, in another window."
  ;;   (interactive)
  ;;   (find-file-other-window (concat org-config-path filename)))

  ;; (defmacro find-config-file (filename)
  ;;   "Edit the elfeed-mode config file, in another window."
  ;;   (interactive)
  ;;   (find-file (concat org-config-path filename)))

#+end_src

;; * Instant Access to wiki index

(setq wiki-init-file (concat org-notes-directory "index.org"))

;; (defun find-wiki-init-file-other ()
;;   "Edit the `wiki-init-file', in another window."
;;   (interactive)
;;   (find-file-other-window wiki-init-file))

;; (defun find-wiki-init-file ()
;;   "Edit the `wiki-init-file', in another window."
;;   (interactive)
;;   (find-file wiki-init-file))
;; ;; (global-set-key (kbd "C-x W") 'find-wiki-init-file)
;; ;; (global-set-key (kbd "C-x w") 'find-wiki-init-file-other)

;; * Miscellaneous

(defun auto-fold(str)
  (re-search-forward str)
  (orgstruct-hijacker-org-shifttab-4 t)
  (orgstruct-hijacker-org-shifttab-4 t))

* Beautify round quotes

Based on [[http://endlessparentheses.com/prettify-your-quotation-marks.html][Prettify your Quotation Marks]].

#+begin_src emacs-lisp
  (defun endless/round-quotes (italicize)
    "Insert “” and leave point in the middle.
    With prefix argument ITALICIZE, insert /“”/ instead (meant
    for org-mode)."
    (interactive "P")
    (if (looking-at "”[/=_\\*]?")
        (goto-char (match-end 0))
      (when italicize
        (if (derived-mode-p 'markdown-mode)
            (insert "__")
          (insert "//"))
        (forward-char -1))
      (insert "“”")
      (forward-char -1)))

  (defmacro endless/define-conditional-key (keymap key def
                                                   &rest body)
    "In KEYMAP, define key sequence KEY as DEF conditionally.
  This is like `define-key', except the definition
  \"disappears\" whenever BODY evaluates to nil."
    (declare (indent 3)
             (debug (form form form &rest sexp)))
    `(define-key ,keymap ,key
       '(menu-item
         ,(format "maybe-%s" (or (car (cdr-safe def)) def))
         nil
         :filter (lambda (&optional _)
                   (when ,(macroexp-progn body)
                     ,def)))))
#+end_src

* Fix apostrophes

Based on [[http://endlessparentheses.com/prettify-you-apostrophes.html][Prettify your Apostrophes]].

#+begin_src emacs-lisp
  (defun endless/apostrophe (opening)
    "Insert ’ in prose or `self-insert-command' in code.
  With prefix argument OPENING, insert ‘’ instead and
  leave point in the middle.
  Inside a code-block, just call `self-insert-command'."
    (interactive "P")
    (if (and (derived-mode-p 'org-mode)
             (org-in-block-p '("src" "latex" "html")))
        (call-interactively #'self-insert-command)
      (if (looking-at "['’][=_/\\*]?")
          (goto-char (match-end 0))
        (if (null opening)
            (insert "’")
          (insert "‘’")
          (forward-char -1)))))
#+end_src

* Check online

Network on

#+begin_src emacs-lisp
  (defun csb/internet-up-p (&optional host)
    (or (= 0
           (if url-proxy-services
               (call-process "ping" nil nil nil "-n" "1" "-w" "1"
                             (or host "proxy.indra.es"))
             (call-process "ping" nil nil nil "-c" "1" "-W" "1"
                           (or host "www.google.com"))))
        (progn
          (message "no network !!")
          nil)))

  ;; (defun csb/internet-up-p (&optional host) t)
#+end_src

* Faster pop-to-mark command

Based on [[http://endlessparentheses.com/faster-pop-to-mark-command.html][this]] blog post.
When popping the mark, continue popping until the cursor actually moves

#+begin_src emacs-lisp
  (defadvice pop-to-mark-command (around ensure-new-position activate)
    (let ((p (point)))
      (dotimes (i 10)
        (when (= p (point)) ad-do-it))))
  (setq set-mark-command-repeat-pop t)
#+end_src

* Smart comment

#+begin_src emacs-lisp :tangle no
  (use-package smart-comment
    ;;   :init
    ;;   (defun csb/smart-mark (arg)
    ;;     "Smart commenting based on the location of point on line.
    ;; A single ARG is passed along to the function being invoked. Two
    ;; universal arguments invoke `smart-comment-cleanup' which deletes
    ;; all lines marked for deletion."
    ;;     (interactive "*P")
    ;;     (cond ((= 16 (prefix-numeric-value arg))
    ;;            (smart-comment-cleanup))
    ;;           ((and mark-active transient-mark-mode)
    ;;            (smart-comment-mark-region (region-beginning) (region-end) arg))
    ;;           ((looking-at "\\s-*$")
    ;;            (funcall smart-comment-mark-line arg))
    ;;           ((smart-comment-is-at-beg)
    ;;            (funcall smart-comment-mark-line arg))
    ;;           (t (funcall smart-comment-mark-line arg))))
    :commands (smart-comment)
    :bind
    ("M-;" . smart-comment)
    ;; ("C-;" . csb/smart-mark)
    )
#+end_src

* Trailer

#+begin_src emacs-lisp
  (provide 'csb-miscellaneous)

  (message (format "Loaded csb-miscellaneous !"))

  ;;; csb-miscellaneous.el ends here
#+end_src

* Old code

;; (defun insert_command(command)
;;   (insert command)
;;   (eshell-send-input)
;; )

;; (defun my_byte_recompile ()
;;   (interactive)
;;   (byte-recompile-directory "~/Dropbox/.emacs.d" 0)
;;   ;; (byte-recompile-directory "~/elisp" 0)
;;   )


;; (defun add-comments ()
;;   "Add two semicolons"
;;   (interactive)
;;   (newline-and-indent)
;;   (insert ";;")
;;   (newline-and-indent)
;;   (insert ";; ")
;;   (newline-and-indent)
;;   (insert ";;")
;;   (previous-line)
;;   (insert "  ")
;;   )


;; (defun add-comments_2 ()
;;   "Add two semicolons"
;;   (interactive)
;;   (newline-and-indent)
;;   (insert ";;")
;;   ;; (newline-and-indent)
;;   ;; (insert ";; ")
;;   ;; (newline-and-indent)
;;   ;; (insert ";;")
;;   ;; (previous-line)
;;   ;; (insert "  ")
;;   )

;; (global-set-key (kbd "C-c x") 'add-comments)
;; (global-set-key (kbd "C-c x") 'add-comments_2)


;; (defun MyInit()
;;   "Config Macro for Org Project TEMPLATE.
;; Latex setup in .dir-locals.el in by dir local variables"
;;   (interactive)

;;   ;; Buffers ______________________________
;;   (OpenBufferAbsolute user-init-file "Init.el")
;;   (garak)
;;   (garak-cmd-connect "cayetano.santos.buendia@gmail.com")
;;   (garak-cmd-status "available")
;;   (garak-gui)
;;   (irc nil)
;;   ;; (circe-maybe-connect "Freenode")

;;   )
;;    ;; (
;;    ;;  (executable-find "~/Dropbox/scripts/rcirc_notify_command.sh")
;;    ;;  (start-process "page-me" nil
;;    ;;                 "~/Dropbox/scripts/rcirc_notify_command.sh" "rcIRC" msg)
;;    ;;  )

;; (defun MySetup()
;;   "Config Macro for Org Project TEMPLATE.
;; Latex setup in .dir-locals.el in by dir local variables"
;;   (interactive)

;;   ;; Tabs ______________________________

;;   (elscreen-kill-others 0)

;; ;; ;; i) current tab -> init.el file
;; ;;   (new_tab "INIT" nil)
;; ;;   (delete-other-windows)
;; ;;   (set-window-buffer-in-frame 0 0 (get-buffer "Init.el"))

;; ;;   ;; ii) new tab -> IM
;; ;;   (new_tab "IM" 1)
;; ;;   (split-window-multiple-ways 2 1)
;; ;;   (set-window-buffer-in-frame 0 0 (get-buffer "*garak*"))
;; ;;   (set-window-buffer-in-frame 1 0 (get-buffer "*Garak*"))
;; ;;   ;; (select-window (get-buffer-window "Code: VHDL" t))
;; ;;   ;; (tiling-tile-right)
;; ;;   ;; (balance-windows)
;; ;;   ;; (select-window (get-buffer-window "Code: SDK Apps" t))
;; ;;   ;; (show-buffers-with-major-mode 'rcirc-mode)

;;   ;; ii) new tab -> IRC
;;   (new_tab "IRC" nil)
;;   (split-window-multiple-ways 3 1)
;;   (set-window-buffer-in-frame 0 0 (get-buffer "#emacs@irc.freenode.net"))
;;   (set-window-buffer-in-frame 1 0 (get-buffer "#archlinux@irc.freenode.net"))
;;   (set-window-buffer-in-frame 2 0 (get-buffer "#org-mode@irc.freenode.net"))
;;   (tiling-tile-left)
;;   (balance-windows)

;; )

** Simple Menu

    ;; (defvar csb/cfw-capture-map (make-sparse-keymap "Agenda "))

    ;; (define-key csb/cfw-capture-map (kbd "p")
    ;;   (cons "perso" (lambda()(interactive) (org-capture nil "rp"))))

    ;; (define-key csb/cfw-capture-map (kbd "a")
    ;;   (cons "apc" (lambda()(interactive) (org-capture nil "ra"))))

    ;; (define-key csb/cfw-capture-map (kbd "l")
    ;;   (cons "lisa" (lambda()(interactive) (org-capture nil "rl"))))

    ;; (define-key csb/cfw-capture-map (kbd "e")
    ;;   (cons "ebex" (lambda()(interactive) (org-capture nil "re"))))

    ;; (define-key csb/cfw-capture-map (kbd "q")
    ;;   (cons "qubic" (lambda()(interactive) (org-capture nil "rq"))))

    ;; (define-key csb/cfw-capture-map (kbd "t")
    ;;   (cons "athena" (lambda()(interactive) (org-capture nil "rt"))))

    ;; (define-key csb/cfw-capture-map (kbd "o")
    ;;   (cons "compton" (lambda()(interactive) (org-capture nil "ro"))))

    ;; (define-key csb/cfw-capture-map (kbd "c")
    ;;   (cons "cnrs" (lambda()(interactive) (org-capture nil "rc"))))

    ;; (define-key cfw:calendar-mode-map "c" csb/cfw-capture-map)

** Remove / Completes lists

#+begin_src emacs-lisp
  (defun csb/list-tosymbol (mylist)
    "Convert a list of strings to a list of symbols."
    (mapcar 'intern mylist))
  ;; (let ((value))
  ;;   (dolist (element mylist value)
  ;;     (setq value (add-to-list 'value (intern element)))))

  (defun csb/list-tostring (mylist)
    "Convert a list of symbols to a list of strings."
    (mapconcat 'symbol-name mylist " "))
  ;; (let ((value " "))
  ;;   (dolist (element mylist value)
  ;;     (setq value (concat (symbol-name element) " " (eval value))))
  ;;   (setq value (substring value 0 -2)))

  (defun csb/list-remove (list-active)
    "Remove an item from a list."
    (interactive)
    ;; (require 'cl-lib)
    (when list-active
      (let ((some-helm-source
             '((name . "HELM at the Emacs")
               (candidates . list-active)
               (action . (lambda (candidate)
                           (helm-marked-candidates))))))
        (dolist (element (helm :sources '(some-helm-source)) value)
          (setq value (remove (intern element) list-active))))))

  (defun csb/list-add (list-active mydiff)
    "Add an item to a list."
    (interactive)
    ;; (require 'cl-lib)
    (let ((some-helm-source
           '((name . "HELM at the Emacs")
             (candidates . mydiff)
             (action . (lambda (candidate)
                         (helm-marked-candidates))))))
      (when mydiff
        (dolist (element (helm :sources '(some-helm-source)) value)
          (setq value (add-to-list 'list-active element))))))
#+end_src

** Eval-result-overlays in Emacs-lisp

[[http://endlessparentheses.com/eval-result-overlays-in-emacs-lisp.html?source=rss][Eval-result-overlays in Emacs-lisp]]

#+begin_src emacs-lisp :tangle no
  (when (file-exists-p (format "%s/.recoll" (getenv "HOME")))
    (use-package cider
      :no-require t
      :init
      (autoload 'cider--make-result-overlay "cider-overlays")
      :config
      (use-package queue)
      (defun endless/eval-overlay (value point)
        (cider--make-result-overlay (format "%S" value)
          :where point
          :duration 'command)
        ;; Preserve the return value.
        value)

      (advice-add 'eval-region :around
                  (lambda (f beg end &rest r)
                    (endless/eval-overlay
                     (apply f beg end r)
                     end)))

      (advice-add 'eval-last-sexp :filter-return
                  (lambda (r)
                    (endless/eval-overlay r (point))))

      (advice-add 'eval-defun :filter-return
                  (lambda (r)
                    (endless/eval-overlay
                     r
                     (save-excursion
                       (end-of-defun)
                       (point)))))))

  ;; (defun endless/eval-overlay (value point)
  ;;   (cider--make-result-overlay (format "%S" value)
  ;;     :where point
  ;;     :duration 'command)
  ;;   ;; Preserve the return value.
  ;;   value)

  ;; (advice-add 'eval-region :around
  ;;             (lambda (f beg end &rest r)
  ;;               (endless/eval-overlay
  ;;                (apply f beg end r)
  ;;                end)))

  ;; (advice-add 'eval-last-sexp :filter-return
  ;;             (lambda (r)
  ;;               (endless/eval-overlay r (point))))

  ;; (advice-add 'eval-defun :filter-return
  ;;             (lambda (r)
  ;;               (endless/eval-overlay
  ;;                r
  ;;                (save-excursion
  ;;                  (end-of-defun)
  ;;                  (point)))))

  ;; (autoload 'cider--make-result-overlay "cider-overlays")
#+end_src
