:PROPERTIES:
:ID:       4185e369-46be-4d8b-a59b-787c58dd9d47
:header-args: :mkdirp yes :tangle-mode (identity #o444)
:END:
#+title: emacs vhdl language
#+filetags: :emacs:vhdl:language:

Emacs vhdl language related functionality.

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#header][Header]]
- [[#generic-after-jump-function][Generic after jump function]]
- [[#vhdl-tools][VHDL tools]]
  - [[#vhdl-tools-mode][VHDL tools mode]]
  - [[#vhdl-tools-vorg-mode][VHDL tools vorg mode]]
    - [[#variables][Variables]]
    - [[#hooks][Hooks]]
- [[#vdhl-mode][VDHL Mode]]
  - [[#notes][Notes]]
  - [[#variables-1][Variables]]
  - [[#custom-binds][Custom binds]]
  - [[#syntax-check-with-flycheck][Syntax check with flycheck]]
    - [[#ghdl][Ghdl]]
    - [[#vhdl-tool][VHDL Tool]]
  - [[#completion][Completion]]
    - [[#built-in][Built-in]]
    - [[#company-capf][Company-capf]]
  - [[#lsp][Lsp]]
  - [[#hooks-1][Hooks]]
  - [[#use][Use]]
    - [[#getting-help---------c-c-][Getting Help         C-c ?]]
    - [[#electrification-e---c-c-c-m-c-e][Electrification /e   C-c C-m C-e]]
    - [[#stuttering-s--------c-c-c-m-c-s][Stuttering /s        C-c C-m C-s]]
    - [[#hydra----------------c-c-v][Hydra                C-c v]]
    - [[#insertindent--------c-c-c-i][Insert/indent        C-c C-i]]
    - [[#template-------------c-t][Template             C-t]]
    - [[#beautify-------------cm-b][Beautify             C|M-b]]
    - [[#fill-----------------c-f][Fill                 C-f]]
    - [[#port-----------------c-p][Port                 C-p]]
    - [[#align----------------c-a][Align                C-a]]
    - [[#fix------------------c-x][Fix                  C-x]]
    - [[#c-c-c-m--------------models-snippets-templates-modes][C-c C-m              models (snippets templates); modes]]
    - [[#custom][Custom]]
      - [[#update-doxygen][Update doxygen]]
- [[#misc][Misc]]
  - [[#batch-beautify-a-set-of-files][Batch beautify a set of files]]
  - [[#dfsljsdflkj][dfsljsdflkj]]
  - [[#src-code-blocks][Src code blocks]]
  - [[#which-key][Which key]]
  - [[#speedbar][Speedbar]]
- [[#trailer][Trailer]]
- [[#references][References]]

* Introduction

Here I include my personal configuration to deal with vhdl source code.

* Header

#+begin_src emacs-lisp
  ;;;; csb-vhdl.el --- Summary
  ;;
  ;;;; Commentary:
  ;;
  ;; This file contains ...
#+end_src

#+begin_src emacs-lisp
  (eval-when-compile
    (require 'lsp-vhdl nil t))
#+end_src

* Generic after jump function

This function will act after jumping.

#+begin_src emacs-lisp
  (when
      ;; defined in miscellaneous
      (fboundp 'csb/generic-after-jump-advice)
    (defun csb/generic-after-jump-advice-vhdl (&optional arg)
      (cond (;; smartscan
             (eq arg 'smartscan)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            (;; xref
             (eq arg 'xref)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines)
             (vhdl-tools--fold))
            ;; helm
            ((eq arg 'helm)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; compile
            ((eq arg 'compile)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; git-gutter
            ((eq arg 'gitgutter)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; imenu
            ((eq arg 'imenu)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; outline
            ((eq arg 'outline)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; focusin
            ((eq arg 'focusin)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; C-c C-n
            ((eq arg 'CcCn)
             (recenter-top-bottom vhdl-tools-recenter-nb-lines))
            ;; default
            (t
             (recenter-top-bottom vhdl-tools-recenter-nb-lines)))
      (when vhdl-tools-mode
        (when vhdl-tools-manage-folding
          (unless (eq arg 'imenu)
            (vhdl-tools--fold))))
      (recenter-top-bottom vhdl-tools-recenter-nb-lines)
      (let ((beacon-blink-delay 1)
            (beacon-blink-duration 1)
            (beacon-size 60))
        (beacon-blink))))
#+end_src

* VHDL tools

** VHDL tools mode

Custom navigation of vhdl sources, hosted [[https://github.com/csantosb/vhdl-tools][here]], on top of =vhdl-mode=.

#+begin_src emacs-lisp
  (use-package vhdl-tools

    :ensure nil
    :config
    (setq vhdl-tools-manage-folding t
          vhdl-tools-save-before-imenu nil
          vhdl-tools-verbose nil
          vhdl-tools-use-outshine nil
          vhdl-tools-max-lines-disable-features 4000
          vhdl-tools-recenter-nb-lines '(4))

    ;; :bind
    ;; (:map vhdl-tools-mode-map
    ;;       ;; When jumping to vorg, don’t check for config file
    ;;       ("C-c M-^" . (lambda ()
    ;;                      (interactive)
    ;;                      (let ((csb/runorgconfig-enable nil))
    ;;                        (vhdl-tools-vorg-jump-to-vorg)))))

    ;; :hook
    ;; (vhdl-tools-mode . (lambda ()
    ;;                      nil))

    )
#+end_src

** VHDL tools vorg mode
:PROPERTIES:
:header-args: :tangle no
:END:

*** Variables

#+begin_src emacs-lisp
  (setq vhdl-tools-vorg-tangle-comments-link t
        vhdl-tools-vorg-tangle-comment-format-beg "@@@"
        vhdl-tools-vorg-tangle-comment-format-end "@@@")
#+end_src

*** Hooks

Recenter point after jumping with smartscan: I want this feature, but
I don’t want to include it in the package.

#+begin_src emacs-lisp
  (add-hook 'vhdl-tools-vorg-mode-hook
            (lambda ()

              (define-key vhdl-tools-vorg-mode-map (kbd "C-c M-,")
                (lambda()(interactive)
                  (let ((vhdl-tools-recenter-nb-lines '(4)))
                    (vhdl-tools-vorg-jump-from-vorg))))

              (define-key vhdl-tools-vorg-mode-map (kbd "C-c M-^")
                (lambda()(interactive)
                  (let ((vhdl-tools-recenter-nb-lines '(4)))
                    (vhdl-tools-vorg-jump-to-vorg))))

              (setq-local csb/runorgconfig-enable nil)
              (setq-local csb/toc-org-enable t)

              (smartparens-mode -1)

              ;; (define-key vhdl-tools-vorg-mode-map [remap smartscan-symbol-go-forward]
              ;;   (lambda()(interactive)
              ;;     (let ((vhdl-tools-recenter-nb-lines '(4)))
              ;;       (vhdl-tools-vorg-smcn-next))))

              ;; (define-key vhdl-tools-vorg-mode-map [remap smartscan-symbol-go-backward]
              ;;   (lambda()(interactive)
              ;;     (let ((vhdl-tools-recenter-nb-lines '(4)))
              ;;       (vhdl-tools-vorg-smcn-prev))))
              ))
#+end_src

* VDHL Mode

** Notes

Not that =vhdl-mode= is derived from =prog-mode=, check with

#+begin_src emacs-lisp :tangle no
  (derived-mode-p 'vhdl-mode 'prog-mode)
#+end_src

in any vhdl buffer, so all setup to =prog-mode= applies here too.

** Variables

Following standard rules

#+begin_src emacs-lisp
  (setq vhdl-basic-offset 2
        vhdl-standard (quote (08 nil))
        vhdl-compiler "Xilinx XST"
        vhdl-forbidden-words '("toto")
        vhdl-beautify-options '(t t t t nil)
        vhdl-highlight-forbidden-words t
        vhdl-highlight-special-words t
        vhdl-highlight-translate-off t
        vhdl-upper-case-constants t  ; Non-nil means convert upper case.
        vhdl-upper-case-types nil
        vhdl-upper-case-keywords nil
        vhdl-upper-case-attributes nil
        vhdl-upper-case-enum-values nil
        ;; Get rid of remove-alist
        vhdl-imenu-generic-expression
        (let ((toto (delq (assoc "Instance" vhdl-imenu-generic-expression) vhdl-imenu-generic-expression)))
          (add-to-list 'toto
                       '("Instance"
                         "^\\s-*\\(\\(\\w\\|\\s_\\)+\\s-*:\\(\\s-\\|\n\\)*\\(entity\\s-+\\(\\w\\|\\s_\\)+\\.\\)?\\(\\w\\|\\s_\\)+\\)\\(.\\)*\\(\\s-\\|\n\\)+\\(generic\\|port\\)\\s-+map\\>"
                         1))))
#+end_src

** Custom binds

#+begin_src emacs-lisp
  (define-key vhdl-mode-map (kbd "C-j") #'vhdl-electric-return)
  (define-key vhdl-mode-map (kbd "s-SPC") #'just-one-space)
  (define-key vhdl-mode-map (kbd "C-c C-k") nil) ; vhdl-compile
#+end_src

** Syntax check with flycheck

*** Ghdl

The following allows using =flycheck= for code checking with =ghdl= as a backend.

Here I customize the default =vhdl-ghdl= checker already included with latest Flycheck.

#+begin_src emacs-lisp
  (when (executable-find "ghdl")
    (progn
      (flycheck-define-checker vhdl-ghdl-Vivado
        "A VHDL syntax checker using ghdl."
        :command ("ghdl"
                  "-s"			; only do the syntax checking
                  "--std=08"
                  "--ieee=synopsys"
                  ;; "-fexplicit"
                  "--workdir=work"
                  "-P/opt/CompiledLibraries/Vivado"
                  "--warn-unused"
                  source)
        :error-patterns
        ((warning line-start (file-name) ":" line ":" column ":w:" (message) line-end)
         (error line-start (file-name) ":" line ":" column ": " (message) line-end))
        :modes vhdl-mode)
      (add-to-list 'flycheck-checkers 'vhdl-ghdl-Vivado)))
#+end_src

#+begin_src emacs-lisp
  (when (executable-find "ghdl")
    (progn
      (flycheck-define-checker vhdl-ghdl-ISE
        "A VHDL syntax checker using ghdl."
        :command ("ghdl"
                  "-s"			; only do the syntax checking
                  "--std=08"
                  "--ieee=synopsys"
                  ;; "-fexplicit"
                  "--workdir=work"
                  "-P/opt/CompiledLibraries/ISE"
                  "--warn-unused"
                  source)
        :error-patterns
        ((warning line-start (file-name) ":" line ":" column ":w:" (message) line-end)
         (error line-start (file-name) ":" line ":" column ": " (message) line-end))
        :modes vhdl-mode))
    (add-to-list 'flycheck-checkers 'vhdl-ghdl-ISE))
#+end_src

*** VHDL Tool

The following allows using =flycheck= for code checking with =vhdl-tool= as a backend.

#+begin_src emacs-lisp :tangle no
  (when (executable-find "vhdl-tool")
    (progn
      (flycheck-define-checker vhdl-vhdltool
        "A VHDL syntax checker, type checker and linter using VHDL-Tool.
  See URL `http://vhdltool.com' and `vhdl-tool client lint -h'"
        :command ("/usr/bin/vhdl-tool" "client" "lint" "--compact" "--stdin" "-f" source)
        :standard-input t
        :error-patterns
        ((warning line-start (file-name) ":" line ":" column ":w:" (message) line-end)
         (error line-start (file-name) ":" line ":" column ":e:" (message) line-end))
        :modes vhdl-mode)
      (add-to-list 'flycheck-checkers 'vhdl-vhdltool)))
#+end_src

** Lsp

#+begin_src emacs-lisp
  ;; (when (not (boundp 'ghdl-ls-bin-name))
  ;;   (defvar ghdl-ls-bin-name "ghdl-ls" "GHDL ls executable"))

  ;;; When lsp client is available
  (when (and
         (require 'lsp-vhdl nil t)
         (or (executable-find vhdl-tool-bin-name)
             (executable-find hdl-checker-bin-name)
             (executable-find vhdl-ls-bin-name)
             (executable-find ghdl-ls-bin-name)))

    ;;;; Select lsp server
    (cond
     ;; using vhdl-tool
     ((executable-find "vhdl-tool")
      (setq lsp-vhdl-server 'vhdl-tool
            lsp-vhdl-server-path (executable-find vhdl-tool-bin-name)
            lsp-vhdl--params nil))
     ;; using ghdl-ls
     ((and (executable-find "ghdl")
           (executable-find "ghdl-ls"))
      (setq lsp-vhdl-server 'ghdl-ls
            lsp-vhdl-server-path (executable-find ghdl-ls-bin-name)
            lsp-vhdl--params nil))
     ;; using hdl_checker
     ((and (executable-find "ghdl")
           (executable-find "hdl_checker"))
      (setq lsp-vhdl-server 'hdl-checker
            lsp-vhdl-server-path (executable-find hdl-checker-bin-name)
            lsp-vhdl--params nil))
     ;; using vhdl-ls
     ((executable-find "vhdl-ls")
      (setq lsp-vhdl-server 'hdl-checker
            lsp-vhdl-server-path (executable-find vhdl-ls-bin-name)
            lsp-vhdl--params nil)))

    ;;;; Update variables
    (require 'lsp-vhdl nil t)
    (lsp-vhdl--set-server-path)
    (lsp-vhdl--set-server-args)

    ;;;; Activate lsp locally following number of lines in buffer
    (add-hook 'vhdl-mode-hook
              (lambda ()
                (when
                    (or
                     (not (boundp 'vhdl-tools-max-lines-disable-features))
                     (< (count-lines 1 (point-max))
                        vhdl-tools-max-lines-disable-features))
                  ;; I prefer default one, lsp identifies all in same category
                  (lsp t)
                  ;; Disable lsp ui doc as this is incompatible with folding
                  (setq-local lsp-ui-doc-delay 600)
                  (setq-local lsp-eldoc-render-all t)
                  ;; doesn't work anyway ...
                  ;; (lsp-ui-doc-mode -1)
                  ))))
#+end_src

** Completion

*** Built-in

VHDL mode provides out of the box =TAB= word completion. See help =C-c ?= for details.

*** Company-capf

Emacs [[https://github.com/sh-ow/vhdl-capf][completion]] at point function (capf) for vhdl-mode.

I’m not using here =(vhdl-capf-enable)=, rather I use a custom equivalent

#+begin_src emacs-lisp
  (use-package vhdl-capf
    ;; :pin melpa
    :config
    (require 'company)
    (require 'vhdl-capf)
    (require 'vhdl-mode)
    (setq
     ;; If t, search in _all_ other vhdl-buffers for completions. When number, search in
     ;; the last opened (number+1) vhdl-buffers.
     vhdl-capf-search-vhdl-buffers-for-candidates 0
     vhdl-capf-exclude-common-vhdl-syntax vhdl-keywords)
    :hook
    (vhdl-mode . (lambda ()
                   ;; Use vhdl-capf if no lsp server is available
                   (unless (and (bound-and-true-p lsp-vhdl-server-path)
                                (or
                                 (not (boundp 'vhdl-tools-max-lines-disable-features))
                                 (< (count-lines 1 (point-max))
                                    vhdl-tools-max-lines-disable-features)))
                     (add-to-list
                      (make-local-variable 'completion-at-point-functions)
                      'vhdl-capf-main)
                     ;; Make ‘company-backends’ local is critcal or else, you will
                     ;; have completion in every major mode
                     (make-local-variable 'company-backends)
                     ;; set ‘company-capf’ first in list
                     (delq 'company-capf company-backends)
                     (add-to-list 'company-backends 'company-capf)))))
#+end_src

** Hooks

 - I set the =vhdl-project-alist= variable dynamically, so that projects under
   speedbar adapt accordingly

   #+begin_src emacs-lisp :tangle no
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 (when (and (buffer-file-name)
                            (boundp 'vhdl-project-alist))
                   (setq vhdl-project-alist
                         `(,(list
                             (persp-name (persp-curr))
                             "Test"
                             (vc-find-root (buffer-file-name) ".git")
                             '("-r ./")
                             "" nil "\\1/" "work" "\\1/work/" "Makefile" ""))))))
   #+end_src


 - For big files, disable a few modes, disable outshine folding and switch back
   to vhdl-93/02, otherwise indenting becomes very slow

   #+begin_src emacs-lisp
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 (when (and
                        (boundp 'vhdl-tools-max-lines-disable-features)
                        (> (count-lines 1 (point-max))
                           vhdl-tools-max-lines-disable-features))
                   (progn
                     (when (boundp 'git-gutter-mode)
                       (git-gutter-mode -1))
                     ;; (when (boundp 'aggressive-indent-mode)
                     ;;   (aggressive-indent-mode -1))
                     (setq-local vhdl-tools-use-outshine nil)
                     (setq-local vhdl-standard (quote (93 nil)))))))
   #+end_src

 - Default syntax checker is =vhdl-ghdl=

   #+begin_src emacs-lisp
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 (flycheck-select-checker
                  (cond ((executable-find "ghdl")
                         'vhdl-ghdl-Vivado)
                        ((executable-find "vhdl-tool")
                         'vhdl-vhdltool)
                        (t
                         'vhdl-ghdl)))))
   #+end_src

 - Enable stuterring and electric modes by default

   #+begin_src emacs-lisp
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 (when (not (get-char-property (point) 'src-block))
                   (vhdl-tools-mode 1))
                 (vhdl-stutter-mode 1)
                 (vhdl-electric-mode 1)))
   #+end_src

 - Hide lines

   #+begin_src emacs-lisp
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 (setq csb/hide-lines-regexp "^\\s-*--!.*$")))
   #+end_src

 - Remaining hooks

   #+begin_src emacs-lisp
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 ;; (font-lock-add-keywords  nil '(("\\`.*\\@.*\\`" 1
                 ;; font-lock-warning-face t)))
                 ;; avoid "before first heading message"
                 ;; in org src blocks
                 ;; (when (not (get-char-property (point) 'src-block))
                 ;;  (outline-hide-sublevels 5))
                 ;; (auto-fill-mode -1)
                 (setq-local next-error-verbose nil)
                 (setq-local  lsp-eldoc-enable-hover nil)
                 ;; (when (boundp 'smartparens-mode)
                 ;;   (smartparens-mode -1))
                 ;; (electric-pair-mode -1)
                 (superword-mode t)
                 (setq-local helm-rg-default-glob-string "*.vhd")
                 ;; (when (fboundp 'highlight-indentation-set-offset)
                 ;;   (highlight-indentation-set-offset vhdl-basic-offset))
                 ;; (when (fboundp 'indent-guide-mode)
                 ;;   (indent-guide-mode t))
                 ;; For some reason, disables electric mode ??
                 (save-place-mode -1)))
   #+end_src

 #+begin_src emacs-lisp :tangle no
  (defun csb/vhdl-tools-imenu-instance-advice-around ()
    )

  (advice-add 'vhdl-tools-imenu-instance :around
              #'csb/vhdl-tools-imenu-instance-advice-around)

 #+end_src

#+begin_src emacs-lisp :tangle no
  (defun csb/vhdl-electric-space-advice-before (count)
    (sp-local-pair 'vhdl-mode "(" nil :actions nil))

  (defun csb/vhdl-electric-space-advice-after (count)
    (sp-local-pair 'vhdl-mode "(" nil :actions '(insert)))

  (defun csb/vhdl-electric-space-advice-around (orig-fun &rest args)
    (interactive "p")
    (sp-local-pair 'vhdl-mode "(" nil :actions nil)
    (apply orig-fun args )
    (sp-local-pair 'vhdl-mode "(" nil :actions '(insert wrap autoskip navigate)))

  (advice-add 'vhdl-electric-space :before #'csb/vhdl-electric-space-advice-before)
  (advice-add 'vhdl-electric-space :after #'csb/vhdl-electric-space-advice-after)

  (advice-add 'vhdl-electric-space :around
              #'csb/vhdl-electric-space-advice-around)
  (advice-remove 'vhdl-electric-space #'csb/vhdl-electric-space-advice-around)

#+end_src

#+begin_src emacs-lisp :tangle no
  ;; (turn-on-orgstruct++)
  ;; (setq-local orgstruct-heading-prefix-regexp "^ *--! ")
  ;; (setq-local orgstruct-heading-prefix-regexp "^ *-- *\\(\\*.*\\)")
  ;; (ispell-minor-mode 1)
  ;; *
  ;; (add-to-list 'imenu-generic-expression '("@" "^ *--! *\\(\\*.*\\)" 1))
  ;; (add-to-list 'imenu-generic-expression '("@" "^\\s-*--!\\s-+\\(\\**\\)\\s-+@brief\\s-+\\(.+\\)" 1))
  ;; (setq imenu-generic-expression '(("@" "^\\s-*--!\\s-+\\(\\**\\)\\s-+@brief\\s-+\\(.+\\)" 2)))
  ;; instances
  ;; (add-to-list 'imenu-generic-expression '("@" "^\\s-*\\(\\(\\w\\|\\s_\\)+\\s-*:\\(\\s-\\|\n\\)*\\(entity\\s-+\\(\\w\\|\\s_\\)+\\.\\)?\\(\\w\\|\\s_\\)+\\)\\(\\s-\\|\n\\)+\\(generic\\|port\\)\\s-+map\\>" 1))
  ;; processes
  ;; (add-to-list 'imenu-generic-expression '("@"  "^\\s-*\\(\\(\\w\\|\\s_\\)+\\)\\s-*:\\(\\s-\\|\n\\)*\\(\\(postponed\\s-+\\|\\)process\\)" 1))
  ;; (setq imenu-generic-expression (delq (assoc "Process" imenu-generic-expression) imenu-generic-expression))
  ;; (setq imenu-generic-expression (delq (assoc "Instance" imenu-generic-expression) imenu-generic-expression))
#+end_src

 - Outshine

   #+begin_src emacs-lisp :tangle no
     (add-hook 'vhdl-mode-hook
               (lambda ()
                 (setq-local outshine-startup-folded-p t)))
   #+end_src

   #+begin_src emacs-lisp :tangle no
     (add-hook 'aggressive-indent-mode-hook
               (lambda ()
                 (when (string= major-mode "vhdl-mode")
                   (remove-hook 'before-save-hook
                                #'aggressive-indent--proccess-changed-list-and-indent 'local))))
   #+end_src

** Use

Main map is =vhdl-mode-map=.

*** Getting Help         C-c ?

Disable default bind to get help, ending by =C-h=, as this disables =desbinds=.

Now, for example =C-c C-t= followed by =C-h= will show all templates, of them with =Helm=.

#+begin_src emacs-lisp
  (define-key vhdl-mode-map "\C-c\C-h" nil) ;; vhdl-doc-mode
  (define-key vhdl-mode-map (kbd "C-c ?") 'vhdl-doc-mode)
#+end_src

*** Electrification /e   C-c C-m C-e

 - =SPC=   :: keyword driven auto template inserting system
 - =s-SPC= :: inserts a space without calling the template

*** Stuttering /s        C-c C-m C-s

Double striking of some keys inserts cumbersome VHDL syntax elements.

*** Hydra                C-c v

Entrance point. Gives access to all =vhdl-mode= related.

#+begin_src emacs-lisp
  (define-key vhdl-mode-map (kbd "C-c v")
    (defhydra csb/hydra-vhdl (:color blue)
      "
    _i_ndent   _I_nsert   _t_emplate   _f_ill   _b_eautify   _p_ort  _c_ompose   _F_ix  _m_odel  _M_ode   _a_lign  _?_
      "
      ("i" (with-initial-minibuffer "vhdl indent") nil)
      ("I" (with-initial-minibuffer "vhdl insert") nil)
      ("t" (with-initial-minibuffer "vhdl template") nil)
      ("f" (with-initial-minibuffer "vhdl fill") nil)
      ("b" (with-initial-minibuffer "vhdl beautify") nil)
      ("a" (with-initial-minibuffer "vhdl align") nil)
      ("p" (with-initial-minibuffer "vhdl -port") nil)
      ("c" (with-initial-minibuffer "vhdl compose") nil)
      ("F" (with-initial-minibuffer "vhdl fix") nil)
      ("m" (with-initial-minibuffer "vhdl model") nil)
      ("M" (with-initial-minibuffer "vhdl mode$") nil)
      ("?" vhdl-doc-mode nil)
      ("q" nil nil)))
#+end_src

*** Insert/indent        C-c C-i

#+begin_src emacs-lisp :tangle no
  (define-key vhdl-mode-map (kbd "C-M-\\") nil) ;; 'vhdl-indent-region
  (define-key vhdl-mode-map (kbd "C-c C-i C-r") 'vhdl-indent-region)
  ;;
  (define-key vhdl-mode-map (kbd "C-M-q") nil) ;; 'vhdl-indent-sexp
  (define-key vhdl-mode-map (kbd "C-c C-i s") 'vhdl-indent-sexp)
  ;;
  (define-key vhdl-mode-map (kbd "C-c C-i C-l") 'vhdl-indent-line)
#+end_src

  (define-key vhdl-mode-map (kbd "C-c C-i")
    '(lambda () (interactive)
       (with-initial-minibuffer "vhdl-.*\\(indent\\).*")
       ))

(define-key vhdl-mode-map (kbd "C-c M-i")
    '(lambda () (interactive)
       (with-initial-minibuffer "vhdl-.*\\(insert\\).*")
       ))

*** Template             C-c C-t

Defined in =vhdl-template-map=.

Disable any bind ending by =C-h=, as this disables binding help.
Now, =C-c C-t= followed by =C-h= will show all of them with =Helm=.

#+begin_src emacs-lisp
  (define-key vhdl-template-map (kbd "ic") #'vhdl-template-insert-construct)
  (define-key vhdl-template-map (kbd "id") #'vhdl-template-insert-directive)
  (define-key vhdl-template-map (kbd "ip") #'vhdl-template-insert-package)
  (define-key vhdl-template-map "\C-h" nil) ;; default to vhdl-template-header
#+end_src

At the combo =C-c C-t= locally to =which-key=

#+begin_src emacs-lisp
  (add-hook 'vhdl-tools-mode-hook
            (lambda ()
              (which-key-mode 1)
              (add-to-list (make-local-variable 'which-key-allow-regexps)
                           "C-c C-t")))
#+end_src

*** Beautify             C|M-b

#+begin_src emacs-lisp
  (defvar csb/vhdl-beautify-shift-semicolom nil
    "When non nil, beautify pushes semicolon to column 79. Indra compliant ...")
#+end_src

#+begin_src emacs-lisp
  (defvar csb/vhdl-shift-semicolom-value 79
    "When non nil, beautify pushes semicolon to column 79. Indra compliant ...")
#+end_src

#+begin_src emacs-lisp
  (defun csb/vhdl-shift-semicolom (&optional start end)
    (when csb/vhdl-beautify-shift-semicolom
      (save-excursion
        (goto-char (or start (point-min)))
        (while (re-search-forward "[,;]$"
                                  (if (region-active-p)
                                      (region-end)
                                    (point-max))
                                  t)
          (backward-char)
          (indent-to-column csb/vhdl-shift-semicolom-value)
          (forward-char)))))
#+end_src

#+begin_src emacs-lisp
  (advice-add 'vhdl-beautify-region :after #'csb/vhdl-shift-semicolom)
#+end_src

#+begin_src emacs-lisp
  (defun csb/vhdl-beautify-region-or-buffer (arg)
    "Call beautify-region, activating region around current paragraph if not already active.
                With a prefix ARG, call beautify-buffer instead.
                In all cases, use vsg too when available."
    (interactive "P")
    (if (equal arg '(4))
        ;; i. whole buffer
        (progn
          ;; apply aggresive indenting
          (aggressive-indent-indent-region-and-on (point-min) (point-max))
          ;; auto-saves buffer
          (vhdl-beautify-buffer))
      (save-excursion
        (when (not (region-active-p))
          (mark-paragraph))
        ;; ii. apply vhdl beautify region
        (aggressive-indent-indent-region-and-on (region-beginning) (region-end))
        (vhdl-beautify-region (region-beginning) (region-end)))))

  (defun csb/vhdl-vsg-beautify-region-or-buffer (arg)
    "Call beautify-region, activating region around current paragraph if not already active.
                With a prefix ARG, call beautify-buffer instead.
                In all cases, use vsg too when available."
    (interactive "P")
    (let ((vsg-config-file
           (format "%s/vsg/config.yaml" (getenv "EDA_COMMON_DIR"))))
      (if (equal arg '(4))
          ;; i. apply vhdl style guide to whole buffer
          (when
              (and (executable-find "vsg")
                   (file-exists-p vsg-config-file))
            (progn
              (save-buffer)
              (shell-command
               (format "vsg -f %s -c %s --fix" (buffer-file-name) vsg-config-file))
              (revert-buffer nil t)
              (with-temp-message "Applying ancillay formatting."
                ;; apply aggresive indenting
                (aggressive-indent-indent-region-and-on (point-min) (point-max))
                ;; auto-saves buffer
                (vhdl-beautify-buffer))))
        ;; ii. apply apply vhdl style guide on active region or current paragraph
        (let ((tmp_filename (make-temp-file "")))
          (when
              (and (executable-find "vsg")
                   (file-exists-p vsg-config-file))
            (save-excursion
              (when (not (region-active-p))
                (mark-paragraph))
              (write-region (region-beginning) (region-end) tmp_filename)
              (shell-command
               (format "vsg -f %s -c %s --fix" tmp_filename vsg-config-file))
              ;; `resize-mini-windows' and `max-mini-window-height
              (delete-region (region-beginning) (region-end))
              (insert-file-contents tmp_filename)
              (delete-file tmp_filename)
              (with-temp-message "Applying ancillay formatting."
                (aggressive-indent-indent-region-and-on (region-beginning) (region-end))
                (vhdl-beautify-region (region-beginning) (region-end)))))))))

  (define-key vhdl-tools-mode-map (kbd "C-c M-b") nil)
  (define-key vhdl-mode-map (kbd "C-c C-b") #'csb/vhdl-beautify-region-or-buffer)
  (define-key vhdl-mode-map (kbd "C-c M-b") #'csb/vhdl-vsg-beautify-region-or-buffer)
#+end_src

*** Fill                 C-f

*** Port                 C-p

*** Align                C-a

*** Fix                  C-x

*** C-c C-m              models (snippets templates); modes

Defined in =vhdl-model-alist= and =vhdl-model-map=.

*** Custom

**** Update doxygen

#+begin_src emacs-lisp :tangle no
  (defun csb/vhdl-update-doxygen ()
    (interactive)
    (async-shell-command "doxygen Doxyfile")
    (other-window 1)
    (delete-window))

  (define-key vhdl-mode-map (kbd "C-c M-ç") #'csb/vhdl-update-doxygen)
#+end_src

* Misc
** Batch beautify a set of files

#+begin_src sh
  for file in *.vhd; do
      emacs "$file" --batch --eval="(progn (setq vhdl-basic-offset 3)(delete-trailing-whitespace)(vhdl-beautify-buffer))";
  done
#+end_src

From [[https://stackoverflow.com/questions/15065010/how-to-perform-a-for-each-file-loop-by-using-find-in-shell-bash][stackoverflow]] I got this, ans I created a [[file:/software.cat/zsh.cat/zsh-plugin-vhdl::*Batch%20beautify][function]]

#+begin_src sh
  find . -type f -iname "*.vhd" -print0 | while IFS= read -r -d $'\0' line; do
      echo "$line\n"
      emacs "$line" --batch --eval="(progn (require 'vhdl-mode)(setq vhdl-basic-offset 3)(delete-trailing-whitespace)(vhdl-beautify-buffer))";
  done
#+end_src

#+begin_src sh
  find ./ -type f \( -iname \*.c -o -iname \*.h \) -print > gtags.files
#+end_src

For more on this, see [[http://wodric.com/la-commande-find/][Les possibilités de la commande find]].

** dfsljsdflkj

Now, when I export to source files, I want to keep the heading in the source
code, so that I get folding, navigation, etc. as in the original org file.

First, I declare a pre-tangle hook, where I store (only for vhdl src blocks) the
above heading properties in variable =vhdl-tools--current-heading=.

#+begin_src emacs-lisp :tangle no
  (defcustom vhdl-tools--current-heading nil
    "Current heading properties of src block.")

  (setq org-babel-pre-tangle-hook nil)

(defun toto ()
  (interactive)
  (save-excursion
    ;; (setq vhdl-tools--current-heading nil)
    (beginning-of-buffer)
    (let ((mytmpfile (make-temp-file "bla"))
          (myfile (format "%s.vhd" (file-name-sans-extension
                                    (buffer-file-name))))
          (vhdl-tools--current-headingg))
      ;; remove old contents
      (write-region "" nil myfile)
      (while (org-next-block 1)
        (when (string= "vhdl" (car (org-babel-get-src-block-info)))
          (save-excursion
            (org-back-to-heading nil)
            (setq vhdl-tools--current-headingg
                  (car (cdr (org-element-headline-parser (point))))))
          (org-babel-tangle '(4) mytmpfile)
          (with-current-buffer (find-file-noselect myfile)
            (end-of-buffer)
            (insert
             (format "\n-- %s %s\n\n"
                     (if (> (plist-get vhdl-tools--current-headingg ':level) 1)
                         (make-string (- (plist-get vhdl-tools--current-headingg ':level) 1)
                                      ?*)
                       (make-string 1 ?*))
                     (plist-get vhdl-tools--current-headingg ':raw-value)))
            (insert-file-contents mytmpfile)))))))

  (add-to-list
   'org-babel-pre-tangle-hook
   (lambda()
     (save-excursion
       (setq vhdl-tools--current-heading nil)
       (org-next-block 1)
       (when (string= "vhdl" (car (org-babel-get-src-block-info)))
         (org-back-to-heading nil)
         (setq vhdl-tools--current-heading
               (car (cdr (org-element-headline-parser (point)))))))))
#+end_src

Then, when =vhdl-tools--current-heading= is not nil, I use a hook in the body to
insert the heading.

#+begin_src emacs-lisp :tangle no
        (setq org-babel-tangle-body-hook nil)

      (add-to-list
         'org-babel-tangle-body-hook
         (lambda()
           (insert (format "%s" (car (org-babel-get-src-block-info))))
           ))

        (add-to-list
         'org-babel-tangle-body-hook
         (lambda()
           (when (and t
                      vhdl-tools--current-heading)
             (message "ahora")
             (message (plist-get vhdl-tools--current-heading ':raw-value))
             (save-excursion
               (beginning-of-buffer)

  (insert
                (format "\n-- %s %s\n\n"
                        (if (> (plist-get vhdl-tools--current-heading ':level) 1)
                            (make-string (- (plist-get vhdl-tools--current-heading ':level) 1)
                                         ?*)
                          (make-string 1 ?*))
                        (plist-get vhdl-tools--current-heading ':raw-value)))))))
#+end_src

** Src code blocks

 - Backbround color

   #+begin_src emacs-lisp
     (push '("vhdl-tools" (:background "#383838")) org-src-block-faces)
   #+end_src

 - Template expansion

   - add =<vh= for vhdl expansion

     #+begin_src emacs-lisp :tangle no
       (add-to-list 'org-structure-template-alist
                    '("vh" "#+begin_src vhdl-tools\n?\n#+end_src"
                      "<src lang=\"vhdl-tools\">\n?\n</src>"))
     #+end_src

   - add =<hv= for reversed vhdl expansion

     #+begin_src emacs-lisp :tangle no
       (add-to-list 'org-structure-template-alist
                    '("hv" "#+end_src\n\n?\n#+begin_src vhdl-tools"
                      "<src lang=\"vhdl-tools\">\n?\n</src>"))
     #+end_src

** Which key

#+begin_src emacs-lisp
  (require 'which-key)
  (add-to-list 'which-key-allow-regexps "C-x c i")
#+end_src

** Speedbar

#+begin_src emacs-lisp
  (require 'speedbar)

  (define-key speedbar-mode-map "d" (lambda () (interactive)
                                      (speedbar-change-initial-expansion-list
                                       "vhdl directory")))

  (define-key speedbar-mode-map "p" (lambda () (interactive)
                                      (speedbar-change-initial-expansion-list
                                       "vhdl project")))
#+end_src

#+begin_src emacs-lisp
  (define-key vhdl-speedbar-mode-map "j" 'speedbar-next)
  (define-key vhdl-speedbar-mode-map "k" 'speedbar-prev)
  (define-key vhdl-speedbar-mode-map "l" 'speedbar-expand-line)
  (define-key vhdl-speedbar-mode-map "h" 'vhdl-speedbar-contract-level)
  (define-key vhdl-speedbar-mode-map "H" (lambda ()
                                           (interactive)
                                           (speedbar-refresh)
                                           (let ((mypoint (point)))
                                             (vhdl-speedbar-contract-all)
                                             (goto-char mypoint))))
  (define-key vhdl-speedbar-mode-map "L" (lambda ()
                                           (interactive)
                                           (save-excursion
                                             (vhdl-speedbar-expand-all))))
  (define-key vhdl-speedbar-mode-map "G" 'vhdl-speedbar-rescan-hierarchy)
  (define-key vhdl-speedbar-mode-map "TAB" 'speedbar-toggle-line-expansion)
  (define-key vhdl-speedbar-mode-map "R" nil)
  (define-key vhdl-speedbar-mode-map (kbd "C-j") 'speedbar-edit-line)
  ;; (define-key vhdl-speedbar-mode-map "C" 'vhdl-speedbar-port-copy)
  ;; (define-key vhdl-speedbar-mode-map "P" 'vhdl-speedbar-place-component)
  ;; (define-key vhdl-speedbar-mode-map "F" 'vhdl-speedbar-configuration)
  ;; (define-key vhdl-speedbar-mode-map "A" 'vhdl-speedbar-select-mra)
  ;; (define-key vhdl-speedbar-mode-map "K" 'vhdl-speedbar-make-design)
  ;; (define-key vhdl-speedbar-mode-map "S" 'vhdl-save-caches)
#+end_src

* References

 - https://guest.iis.ee.ethz.ch/~zimmi/emacs/vhdl-mode.html
