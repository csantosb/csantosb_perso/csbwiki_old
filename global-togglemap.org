---
format: Org
categories: emacs config togglemap
toc: yes
title: csb-global-launchermap
content: implements a global toggle for emacs apps
...

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#introduction][Introduction]]
- [[#header][Header]]
- [[#keys][Keys]]
  - [[#generic][Generic]]
  - [[#prefix-key][Prefix Key]]
  - [[#toggle-inferior-shell][Toggle inferior shell]]
  - [[#toggle-spell-check][Toggle spell check]]
  - [[#toggle-read-only][Toggle read only]]
  - [[#outline-minor-mode][Outline minor mode]]
- [[#trailer][Trailer]]

* Introduction

Based on [[http://endlessparentheses.com/the-toggle-map-and-wizardry.html][this]] article

* Header

#+begin_src emacs-lisp
  ;;; csb-global-togglemap- --- Summary
  ;;
  ;;; Commentary:
  ;;
  ;; This file contains ...
  ;;
  ;;; Code:
#+end_src

* Keys

** Generic

  (define-key endless/toggle-map "w" (lambda()(interactive)(if global-superword-mode (global-superword-mode -1)(global-superword-mode 1))))

  (define-key endless/toggle-map "d" 'toggle-debug-on-error)
  ;; (define-key endless/toggle-map "f" 'toggle-frame-fullscreen)
  (define-key endless/toggle-map "l" 'toggle-truncate-lines)
  (define-key endless/toggle-map "p" 'paredit-mode)
  (define-key endless/toggle-map "q" 'toggle-debug-on-quit)
  (define-key endless/toggle-map "t" 'endless/toggle-theme)

#+begin_src emacs-lisp
  (defvar whitespace-mode nil)
  (defvar auto-fill-mode nil)
  (defvar-local csb/outline-minor-mode nil)
  (defvar csb/fit-window-enable nil)
  (defvar highlight-changes-visible-mode nil)
  ;;   ("d" csb/toggle-diff nil :color blue)

  ;; (defun csb/mu4e-headers-include-related-toggle()
  ;;   (interactive)
  ;;   (setq mu4e-headers-include-related (not mu4e-headers-include-related)))

  (defun csb/fit-window-enble-toggle()
    (interactive)
    (setq csb/fit-window-enable (not csb/fit-window-enable))
    (if csb/fit-window-enable
        (message "fit window enabled")
      (message "fit window disabled")))
#+end_src

#+begin_src emacs-lisp
  (defhydra csb/hydra-toggle
    (:color red
            ;; :timeout 50
            :idle 1.0
            :pre (progn
                   (when (not (boundp 'follow-mode))
                     (require 'follow))
                   (when (not (boundp 'highlight-indentation))
                     (require 'highlight-indentation))
                   (when (not (boundp 'aggressive-fill-paragraph-mode))
                     (require 'aggressive-fill-paragraph))
                   (when (not (boundp 'display-line-numbers-mode))
                     (require 'display-line-numbers))
                   (when (not (boundp 'mu4e-headers-include-related))
                     (setq mu4e-headers-include-related t ))
                   (when (not (boundp 'annotate-mode))
                     (setq annotate-mode nil ))
                   (require 'smtpmail))  :hint nil)
    "
                       _D_ebug-on-error     %(if debug-on-error 1 0)    _W_hitespace     %(if whitespace-mode 1 0)     ^_H_ changes      %(if highlight-changes-visible-mode 1 0)   f_o_llow mode     %(if follow-mode 1 0)
                       _A_uto-fill          %(if auto-fill-mode 1 0)    _R_ead-only      %(if buffer-read-only 1 0)     _O_ outline      %(if outline-minor-mode 1 0)   fl_y_spell mode   %(if flyspell-mode 1 0)
                       _d_iff               %(if diff-hl-mode 1 0)    ^^_h_igh-indent    %(if highlight-indentation-current-column-mode 1 0)     _i_menu-list     %(if imenu-list-minor-mode  1 0)   _t_ab-bar         %(if tab-bar-mode  1 0)
                       _f_lycheck           %(if flycheck-mode 1 0)    ^_C_olumn-number  %(if column-number-mode 1 0)     ___ modal        %(if modal-mode 1 0)
                       _|_ truncate-lines   %(if truncate-lines 1 0)    _c_ompany        %(if company-mode 1 0)  af-_p_aragraph      %(if aggressive-fill-paragraph-mode 1 0)
                       _T_oc-org            %(if csb/toc-org-enable 1 0)    _F_ringes        %(if csb/window-fringes-enable 1 0)    a_n_otate         %(if annotate-mode 1 0)
                       _w_hich-key          %(if which-key-mode 1 0)    _v_iew mode      %(if view-mode 1 0)     _l_inum mode     %(if display-line-numbers 1 0)

              mu4e:   q_u_eue mail          %(if smtpmail-queue-mail 1 0)    ^_r_elated mail   %(if mu4e-headers-include-related 1 0)
                    "
    ("_" toggle-modal-mode nil)
    ("c" company-mode nil :color blue)
    ("u" mu4e~main-toggle-mail-sending-mode nil)
    ("r" mu4e-headers-toggle-include-related :exit t)
    ("f" (eval csb/toggle-syntax-checker) nil :color blue)
    ("F" csb/window-fringes-enable-toggle nil)
    ;; ("m" (lambda()(interactive) (hide-mode-line-mode)) nil :exit t)
    ;; ("m" hide-mode-line-mode nil :exit t)
    ("m" (lambda()(interactive)
           (if (bound-and-true-p hide-mode-line-mode)
               (turn-off-hide-mode-line-mode)
             (turn-on-hide-mode-line-mode))
           (redraw-display)
           ) nil :exit t)
    ("d" nil)
    ("D" toggle-debug-on-error nil)
    ("o" follow-mode nil :color blue)
    ("y" flyspell-mode nil)
    ("H" highlight-changes-visible-mode nil :color blue)
    ("N" neotree-toggle nil :exit t)
    ("t" (lambda()
           (interactive)
           (if tab-bar-mode
               (tab-bar-mode -1)
             (tab-bar-mode t)))
     nil :exit t)
    ("n" (lambda()(interactive)
           (if annotate-mode
               (progn
                 (message "annotate mode disabled")
                 (annotate-mode -1))
             (progn
               (message "annotate mode enabled")
               (annotate-mode 1)))) nil)
    ;; highlight indentation
    ("h" (lambda ()
           (interactive)
           (if highlight-indentation-current-column-mode
               (progn
                 (highlight-indentation-current-column-mode -1)
                 (highlight-indent-guides-mode -1))
             (progn
               (highlight-indentation-current-column-mode t)
               (highlight-indent-guides-mode t))))  nil)
    ;; auto fill
    ("A" (lambda()(interactive)
           (if auto-fill-function
               (progn
                 (message "auto fill disabled")
                 (setq auto-fill-mode nil)
                 (auto-fill-mode -1))
             (progn
               (message "auto fill enabled")
               (setq auto-fill-mode t)
               (auto-fill-mode)))) nil)
    ("e" csb/shell-pop-eshell nil :exit t) ; bound in eshell
    ("v" csb/shell-pop-vterm nil :exit t) ; bound in vterm
    ("a" csb/term-toggle-ansi-term nil :color blue)
    ("O" (lambda()(interactive)
           (if csb/outline-minor-mode
               (progn
                 (message "outline minor disabled")
                 (setq csb/outline-minor-mode nil)
                 (outline-minor-mode -1))
             (progn
               (message "outline minor enabled")
               (setq csb/outline-minor-mode t)
               (outline-minor-mode t)))) nil)
    ;; ("v" (lambda()(interactive)
    ;;        (when (boundp 'vhdl-tools-vorg-map)
    ;;          (if vhdl-tools-vorg-mode
    ;;              (vhdl-tools-vorg-mode -1)
    ;;            (vhdl-tools-vorg-mode t)))) nil :color blue)
    ("V" view-mode nil :color blue)
    ("s" (call-interactively csb/toggle-inferior-shell) nil :color blue)
    ;; ("i" auto-insert-mode nil)
    ("i" (lambda ()
           (interactive)
           (require 'imenu-list)
           (imenu-list-smart-toggle)) nil :color blue)
    ("|" toggle-truncate-lines nil)
    ("T" csb/toc-org-toggle :color blue)
    ("p" aggressive-fill-paragraph-mode :color blue)
    ("W" whitespace-mode nil)
    ;; ("W" which-key-mode nil)
    ;; ("W" csb/eww-toggle nil)
    ("w" which-key-mode nil)
    ;; ("w" toggle-eww nil :color blue)
    ("R" csb/toggle-read-only :color blue)
    ("C" column-number-mode nil)
    ("l" display-line-numbers-mode nil)
    ("q" nil "quit"))
#+end_src

** Prefix Key

#+begin_src emacs-lisp
  (define-key ctl-x-map "t" nil)
  (define-key ctl-x-map "t" #'csb/hydra-toggle/body)
#+end_src

** Toggle inferior shell

#+begin_src emacs-lisp
  (defvar csb/toggle-inferior-shell 'csb/term-toggle-shell "BLA")
#+end_src

** Toggle spell check

Select spell checker; default is =Flycheck=. This may be overridden by setting
this variable locally (see matlab major mode config file for an example).

#+begin_src emacs-lisp
  (defvar csb/toggle-syntax-checker '(call-interactively 'flycheck-mode) "VLA")
#+end_src

** Toggle read only

Generalized version of `read-only-mode': If the current buffer can be edited
with Wdired, (i.e. the major mode is `dired-mode'), call
`wdired-change-to-wdired-mode'. Otherwise, toggle `read-only-mode'.

#+begin_src emacs-lisp
  (defun csb/toggle-read-only()
    (interactive)
    (cond
     ((string= major-mode "grep-mode")
      (wgrep-change-to-wgrep-mode))
     ((string= major-mode "occur-mode")
      (occur-edit-mode))
     ((string= major-mode "dired-mode")
      (dired-toggle-read-only))
     ((and (boundp 'ggtags-mode) ggtags-mode)
      (ggtags-toggle-project-read-only))
     (t
      (if buffer-read-only
          (read-only-mode -1)
        (read-only-mode 1)))))
#+end_src

** Outline minor mode

#+begin_src emacs-lisp :tangle no
  (define-key csb/hydra-toggle/keymap (kbd "o") (lambda ()
                                             (interactive)
                                             (if outline-minor-mode
                                                 (progn
                                                   (outline-minor-mode 0)
                                                   (local-set-key (kbd "C-c M-t") nil)  ;; navi - remove custom keybind
                                                   (local-set-key (kbd "C-c M-s") nil)  ;; navi - remove custom keybind
                                                   (local-set-key (kbd "M-#") nil)      ;; remove custom keybind
                                                   (global-set-key (kbd "M-#") nil)      ;; remove custom keybind
                                                   ;; (local-set-key (kbd "C-.") nil)
                                                   ;; (local-set-key (kbd "C-c M-t") nil)
                                                   )
                                               (progn
                                                 (outline-minor-mode t)
                                                 (local-set-key (kbd "C-c M-t") 'navi-search-and-switch)      ;; navi - custom key (as org-toc)
                                                 (local-set-key (kbd "C-c M-s") 'navi-switch-to-twin-buffer)  ;; navi - custom key
                                                 (local-set-key (kbd "M-#") 'outorg-edit-as-org)
                                                 (define-key outline-minor-mode-map (kbd "<backtab>") 'outshine-cycle-buffer)
                                                 ;; defaults
                                                 ;; (define-prefix-command 'my-navi-key)
                                                 ;; (local-set-key (kbd "C-.") 'my-navi-key)
                                                 ;; (local-set-key (kbd "C-c M-t") nil)  ;; remove defaults
                                                 ;; (local-set-key (kbd "M-#") nil)      ;; remove defaults
                                                 ;; (define-key my-navi-key (kbd "n") 'navi-search-and-switch)
                                                 ;; (define-key my-navi-key (kbd "s") 'navi-switch-to-twin-buffer)
                                                 ;; (define-key my-navi-key (kbd "o") 'outorg-edit-as-org)
                                                 ))))
#+end_src

* Trailer

#+begin_src emacs-lisp
  (provide 'csb-global-togglemap)
  ;;; csb-global-togglemap.el ends here
#+end_src
