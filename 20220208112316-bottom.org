:PROPERTIES:
:ID:       949d8f9d-27c5-42fe-bd36-2ec7591e7c0d
:END:
#+title: bottom
#+filetags: :sysmon:cli:sw:

A customizable cross-platform graphical process/system monitor for the terminal.

* Tips

Launch using =btm=.

* References

 - https://github.com/ClementTsang/bottom
