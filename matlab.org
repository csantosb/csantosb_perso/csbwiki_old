:PROPERTIES:
:ID:       77fa8a5e-4f7d-4d07-b5ce-02d354af5a1c
:header-args: :mkdirp yes :tangle-mode (identity #o444)
:END:
#+filetags: :matlab:lang:
#+title: matlab

* Table of Contents                                 :TOC:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#matlab][Matlab]]
  - [[#introduction][Introduction]]
    - [[#install][Install]]
    - [[#download][Download]]
    - [[#windows][Windows]]
    - [[#linux][Linux]]
  - [[#autoload][Autoload]]
  - [[#variables][Variables]]
  - [[#keys][Keys]]
    - [[#shell-run][Shell run]]
    - [[#shell-toggle][Shell toggle]]
    - [[#switch-to-the-shell][Switch to the shell]]
    - [[#help][Help]]
    - [[#templates---c-c-c-t][Templates   C-c C-t]]
    - [[#ispell][Ispell]]
    - [[#completion][Completion]]
    - [[#movement][Movement]]
    - [[#other-keys][Other Keys]]
  - [[#hook][Hook]]
  - [[#getting-help][Getting help]]
    - [[#dash][Dash]]
  - [[#syntax-check][Syntax check]]
    - [[#flycheck][Flycheck]]
    - [[#mlint][Mlint]]
      - [[#variables-1][Variables]]
      - [[#keys-1][Keys]]
    - [[#toggle][Toggle]]
  - [[#navigate][Navigate]]
    - [[#outshine][Outshine]]
  - [[#tools][Tools]]
    - [[#export-org-to-matlab][Export org to matlab]]
- [[#matlab-shell][Matlab Shell]]
  - [[#shell-toggle-1][Shell toggle]]
  - [[#variables-2][Variables]]
  - [[#keys-2][Keys]]
  - [[#hooks][Hooks]]
- [[#debugging][Debugging]]
- [[#trailer][Trailer]]
- [[#old-code][Old Code]]

* Emacs configuration                                                 :emacs:

** Matlab

*** Introduction

This is the configuration file corresponding to emacs matlab-mode.

#+begin_src emacs-lisp
  ;; package --- Summary
  ;;
  ;;; Commentary:
  ;;
  ;;; Code:
#+end_src

Eric Ludlam's announce [[http://blogs.mathworks.com/community/2009/09/14/matlab-emacs-integration-is-back/][MATLAB-Emacs integration is back]]

Web site:
[[http://sourceforge.net/projects/matlab-emacs/]],
[[http://matlab-emacs.sourceforge.net/]]

Introductory article at [[http://blog.angjookanazawa.com/post/8815280589/productivity-matlab-emacs-integration-more][one step at a time]]

Installed from el-get recipe, more complete (toolbox folder). Dependency with cedet.

The file startup.m @ ~/Documents/MATLAB contains the following

addpath('/home/csantos/Dropbox/.emacs.d/el-get/matlab-mode/toolbox','-begin');
rehash;
emacsinit('~/Dropbox/myscripts/em');

La version el-get me da problemas con la funcion column ...

Provar M-x 'profiler-start'

**** Install

To install =matlab-emacs= mode run the file 'dl_emacs_support.m' under
"matlab-mode" folder and then makeee

As for [2014-12-09 Tue] I am using the release provided by el-get

[[file:~/.emacs.d/el-get/matlab-mode/README::matlab-emacs%20Project:][Matlab README]]
[[file:~/.emacs.d/el-get/matlab-mode/INSTALL::Installation:][Matlab INSTALL]]

Fixes a problem in mlint.m ~ line 800 : make-local-hook obsolete
(defalias 'make-local-hook 'ignore)
(defalias 'column 'current-column)

**** Download                                                    :encrypt:
-----BEGIN PGP MESSAGE-----
Version: GnuPG v2

hQIMA2fmT3l1x5GPARAAivXjZlczaSlUC6vkElGwXFEUxNs9fo8UrkrvzOFUA241
4AvI/yz4Zq+3cbVVcj/YiXmeo+AFYSkOMy/eNCojKwNtM0Ufb5KCjU990VKEBXeD
Y/x7/BuBEPfH9QatiLP6xlL21dIqzQUzAmytojxvsk8DOjRQ3cw2glIC0/o3AMZX
DLzndjvSmpnBlHk2vDNv3Uc5oOTgPCAryunVGLHnkOftkY+KLdMI3Z/50G4PoNHN
cdn8WDoLKlvDRXDjw12dQYbPk0kRZfCv4M4jdVRpN3RraM4NFx7Pwncijk57ReZY
Ov0giTeYfu9wUtS22Jz8Lw+gLNW/l+U6Tr/H4GYQeRW5uW9DqQhVbsu7VlwnvtY5
s0oQc71Q2yZmXWXlU+v54fPvjBwATi217mmXnWJWqdaO1gKI6jxYMZ1iDv33fdwz
+yTmancFBhR/RiOF36dhgK35lyC9gW9qXMo2Bg3Vq9MR/gsaeo7xSRWjuvfYOIji
Urj9oHO5+26XFajNOhtTRyOCVGYeySZTFwXKSFs4YE5cxpIQIHqMwK6zP6WUWaSn
DLib2LKfZyuSS89w7QljktO0ZrMuf73YNgq8+xSRtVDJjMf4T5sJ9p+QqEHjqZa/
r/4VCeb62XQ4nRKBnDCfU2L2i/+ktIxdmnaR2RxR9Xbesfhntsvjn8cqclGI0LrS
OwHIIOcCwyJEqog5IfALJjKL/4GvAPJ+x4VAy/SMgKqcJVNp1DGBaVAD8d7aatha
mYitS599p0T4c7K9
=VCEN
-----END PGP MESSAGE-----

**** Windows

Both, x86 and x64, one iso file

http://bid.deu.edu.tr/en/software/matlab2014b

**** Linux

*** Autoload

#+begin_src emacs-lisp
  ;; (require 'matlab-load) ;; all autoloads are here
  (require 'ob-matlab)
#+end_src

*** Variables

#+begin_src emacs-lisp
  (setq matlab-comment-column 50                             ;; Goal column for on-line comments
        matlab-comment-region-s "% "
        matlab-indent-function t
        matlab-indent-function-body t                        ;; if you want function bodies indented
        matlab-functions-have-end t
        ;; semanticdb-matlab-include-paths ("/programs/MATLAB/R2014b")
        semantic-matlab-root-directory  "/opt/MATLAB/R2018a"
        matlab-indent-level 4                                ;; Level to indent blocks.
        matlab-return-function 'matlab-indent-end-before-ret ;; customize RET handling with this function
        matlab-fill-code t                                   ;; auto-fill code
        matlab-fill-strings t                                ;; auto-fill strings
        matlab-highlight-block-match-flag t                  ;; enable matching block begin/end keywords
        matlab-vers-on-startup t
        matlab-handle-simulink nil                           ;; enable simulink keyword highlighting
        matlab-auto-fill t                                   ;; do auto-fill at startup
        matlab-functions-have-end nil                        ;; functions terminate with end
        matlab-cont-level 4                                  ;; level to indent continuation lines
        matlab-verify-on-save-flag t                         ;; enable code checks on save
        matlab-cont-requires-ellipsis t                      ;; does your MATLAB support implied elipsis
        ;; matlab-indent-past-arg1-functions                 ;; *Regex describing functions whose first arg is special.
        ;; matlab-maximum-indents                            ;; list of maximum indents during lineups.
        matlab-vers-on-startup nil)
#+end_src

*** Keys

Should be compliant with [[file:emacs.cat/org-config.cat/csb-interactive-shell::*Keys][Keys]].

**** Shell run

#+begin_src emacs-lisp
  (defun csb/matlab-shell-run-region-or-buffer (&optional arg)
    "Run region from BEG to END and display result in MATLAB shell.
  If region is not active run the current paragraph.
  With a prefix ‘ARG' run current line; double prefix run buffer.
  This command requires an active Julia shell, activating one when non
  existing."
    (interactive "P")
    (window-configuration-to-register :matlab-shell-tmp)
    (cond
     ((equal arg '(16))
      (matlab-shell-save-and-go))
     ((equal arg '(4))
      (matlab-shell-run-region-or-line))
     ((use-region-p)
      (matlab-shell-run-region (region-beginning)(region-end)))
     (t
      (progn
        (save-excursion (mark-paragraph))
        (matlab-shell-run-region (region-beginning) (region-end)))))
    (jump-to-register :matlab-shell-tmp)
    (keyboard-quit))
#+end_src

#+begin_src emacs-lisp :tangle no
  (define-key matlab-mode-map (kbd "C-c C-j")
    #'csb/matlab-shell-run-region-or-buffer)
  (define-key matlab-mode-map (kbd "C-c C-c") #'csb/matlab-shell-run-region-or-buffer)  ;; save this M file, and evaluate it in a MATLAB shell
#+end_src

#+begin_src emacs-lisp :tangle no
  ;; (define-key matlab-mode-map (kbd "C-c C-l") #'matlab-shell-run-region-or-line)    ;; send region or line
  (define-key matlab-mode-map (kbd "C-c C-m") #'matlab-shell-run-command)           ;; run COMMAND and display result in a buffer
  (define-key matlab-mode-map (kbd "C-c C-e") #'matlab-shell-run-cell)              ;; run the cell the cursor is in
  (define-key matlab-mode-map [(meta control return)] nil)                         ;; matlab-shell-run-cell
#+end_src

**** Shell toggle

My custom launch / switch to shell.

#+begin_src emacs-lisp
  (csb/toggle-terminals "MATLAB")
#+end_src

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (setq-local csb/toggle-inferior-shell
                          #'csb/term-toggle-MATLAB)))
#+end_src

**** Switch to the shell

#+begin_src emacs-lisp
  (define-key matlab-mode-map (kbd "C-c C-z")
    (lambda ()
      (interactive)
      (matlab-show-matlab-shell-buffer)
      (beacon-blink)))
#+end_src

#+begin_src emacs-lisp
  (define-key csb/hydra-window/keymap (kbd "z")
    (lambda ()
      (interactive)
      (matlab-show-matlab-shell-buffer)
      (beacon-blink)))
#+end_src

**** Help

Remove default C-h C-m

#+begin_src emacs-lisp
  ;; (define-key km [(control h) (control m)] matlab-help-map)
  (define-key matlab-mode-map (kbd "C-h RET")  nil)
#+end_src

Asign to C-c ?

#+begin_src emacs-lisp
  (define-key matlab-help-map "r" nil)       ;; run-command doesn't belong here
  (define-key matlab-mode-map (kbd "C-c ?") matlab-help-map)

  (defadvice matlab-shell-describe-command (around matlab-shell-describe-command-toto activate)
    (window-configuration-to-register :matlab-fullscreen)
    ad-do-it
    (jump-to-register :matlab-fullscreen))
#+end_src

(define-key matlab-help-map (kbd "C-h W") #'matlab-find-file-on-path)

**** Templates   C-c C-t

Remove default C-c C-c

#+begin_src emacs-lisp
  ;; (define-key km [(control c) (control c)] matlab-insert-map)
  (define-key matlab-mode-map (kbd "C-c C-c")  nil)
#+end_src

Assign to C-c C-t

#+begin_src emacs-lisp
  ;; (define-key matlab-insert-map "'" nil)    ;; stringgify-region doesn't belong here
  ;; (define-key matlab-insert-map "\C-s" nil) ;; matlab-ispell-strings doesn't belong here
  ;; (define-key matlab-insert-map "\C-c" nil) ;; matlab-ispell-comments doesn't belong here
  ;; (define-key matlab-mode-map (kbd "C-c C-t") matlab-insert-map)
#+end_src

**** Ispell

#+begin_src emacs-lisp
  (defvar matlab-ispell-map
    (let ((km (make-sparse-keymap)))
      (define-key km "\C-s" 'matlab-ispell-strings)
      (define-key km "\C-c" 'matlab-ispell-comments)
      (define-key km "\C-r" 'matlab-stringgify-region)
      km))

  (define-key matlab-mode-map (kbd "C-c i") matlab-ispell-map)
#+end_src

**** Comment / uncomment / electric  C-c {;:%} - line M-;

#+begin_src emacs-lisp :tangle no

#+end_src

**** Completion

Check [[file:csb-completions.page::*Introduction][this]] file for compatibility.

#+begin_src emacs-lisp :tangle no
  (with-eval-after-load 'matlab
    ;; (define-key matlab-mode-map (kbd "C-M-i")  'matlab-complete-symbol)
    )
  ;; (define-key matlab-mode-map (kbd "TAB")    'matlab-complete-symbol)
#+end_src

**** Movement

;; C-M-a           matlab-beginning-of-defun
;; C-M-e           matlab-end-of-defun
;; C-M-q           matlab-indent-sexp
;; C-M-b           matlab-backward-sexp
;; C-M-f           matlab-forward-sexp
;; C-M-q           matlab-indent-sexp

#+begin_src emacs-lisp :tangle no
  (with-eval-after-load 'matlab)
  ;; Miscellaneous
  ;; (define-key matlab-mode-map (kbd "C-c C-f p") 'matlab-fill-paragraph)
  ;; (define-key matlab-mode-map (kbd "C-c C-f r") 'matlab-fill-region)
#+end_src

**** Other Keys

Indent
matlab-indent-function-body-p
matlab-indent-line
matlab-indent-after-ret
matlab-indent-before-ret
matlab-indent-end-before-ret
C-M-q - Indent syntactic block of code.

;;; *** Another Convenient navigation commands are:

M-a   - Move to the beginning of a command.
M-e   - Move to the end of a command.
C-M-a - Move to the beginning of a function.
C-M-e - Move do the end of a function.
C-M-f - Move forward over a syntactic block of code.
C-M-b - Move backwards over a syntactic block of code.
;;
Convenient editing commands are:
M-q             matlab-fill-paragraph
C-c C-f         matlab-fill-comment-line
C-c C-q         matlab-fill-region
C-c C-f - Fill the current comment line.
C-c C-q - Fill code and comments in region.
M-q     - Refill the current command or comment.
C-M-i   - Symbol completion of matlab symbolsbased on the local syntax.
;;
<return>        matlab-return
;;
M-RET           newline
M-j             matlab-comment-line-break-function
C-c C-j         matlab-justify-line
C-c C-t         matlab-show-line-info
C-c <return>    matlab-comment-return

*** Hook

Matlab-mode is not derived from prog-mode, to check with

#+begin_src emacs-lisp :tangle no
  (derived-mode-p 'prog-mode)
#+end_src

so I have to run the =prog-mode= hook

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (run-hook-with-args 'prog-mode-hook)))
#+end_src

Have a look at =prog-mode-hook=, as these will get loaded before this

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (csb/debug-mode 1)
              (hide-mode-line-mode t)
              ;; (auto-fill-mode 0)
              ;; Enable CEDET feature support for MATLAB code
              (matlab-cedet-setup)
              (mlint-minor-mode t)
              (semantic-mode -1)
              (setq which-func-mode nil)))
#+end_src

*** Getting help

**** Dash

I activate the docset and then set it local in hooks section.

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (setq-local helm-dash-docsets '("MATLAB"))))

  (helm-dash-activate-docset "MATLAB")
#+end_src

*** Syntax check

**** Flycheck

MLint is a Flycheck like syntax checker. The following code allows
using =flycheck= for code checking with =mlint= as a backend.

#+begin_src emacs-lisp
  ;; (if (string= (system-name) "apcnb158")
  ;;     (flycheck-define-command-checker 'matlab-mlint
  ;;       "A Matlab checker based on mlint."
  ;;       :command `(,"/opt/MATLAB/R2018a/bin/glnxa64/mlint" source)
  ;;       :error-patterns
  ;;       '((warning line-start "L " line " (C " (1+ digit)  "): " (message) line-end))
  ;;       :modes '(matlab-mode))
  ;;   (flycheck-define-command-checker 'matlab-mlint
  ;;     "A Matlab checker based on mlint."
  ;;     :command `(,"c:/MATLAB_R2018a/bin/win64/mlint.exe" source)
  ;;     :error-patterns
  ;;     '((warning line-start "L " line " (C " (1+ digit)  "): " (message) line-end))
  ;;     :modes '(matlab-mode)))

  (when (file-directory-p "/opt/MATLAB")
    (flycheck-define-checker matlab-mlint
      "A Matlab checker based on mlint."
      :command ("/opt/MATLAB/R2018a/bin/glnxa64/mlint" source)
      :error-patterns
      ((error line-start "L " line " (C " (1+ digit)  "): " (message) line-end))
      ;; ((error line-start (file-name) ":" line ":" column ": " (message) line-end))
      :modes matlab-mode))

  (add-to-list 'flycheck-checkers 'matlab-mlint)
#+end_src

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (flycheck-select-checker 'matlab-mlint)))
#+end_src

**** Mlint

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (mlint-minor-mode 1)
              (setq-local matlab-show-mlint-warnings t)
              ;; (matlab-toggle-show-mlint-warnings)  ;; MLint ON by default
              ))
#+end_src

***** Variables

#+begin_src emacs-lisp
  (stante-after mlint
                ;; List of possible locations of the mlint program.
                (setq mlint-programs
                      (if running-os-is-linux
                          '("/opt/MATLAB/R2018a/bin/glnxa64/mlint")
                        '("c:/MATLAB_R2018a/bin/win64/mlint.exe"))
                      mlint-program
                      (if running-os-is-linux
                          "/opt/MATLAB/R2018a/bin/glnxa64/mlint"
                        "c:/MATLAB_R2018a/bin/win64/mlint.exe")
                      ;; Non nil if command ‘mlint-minor-mode’ should display
                      ;; messages while running.
                      mlint-verbose t))
#+end_src

#+begin_src emacs-lisp :tangle no
          ;; mlint-calculate-cyclic-complexity-flag
          ;; mlint-error-id-fix-alist
          ;; mlint-flags
          ;; mlint-lm-delete-focus
          ;; mlint-lm-entry
          ;; mlint-lm-entry-depricated
          ;; mlint-lm-entry-logicals
          ;; mlint-lm-entry-unused-argument
          ;; mlint-lm-group
          ;; mlint-lm-quiet
          ;; mlint-lm-replace-focus
          ;; mlint-lm-str2num
          ;; mlint-mark-group
          ;; mlint-minor-menu
          ;; mlint-minor-mode
          ;; mlint-minor-mode-map
          ;; mlint-minor-mode-was-enabled-before
          ;; mlint-output-regex
          ;; mlint-overlay-map
          ;; mlint-platform
          ;; mlint-program-selection-fcn
          ;; mlint-programs
          ;; mlint-scan-for-fixes-flag
          ;; mlint-symtab-info
          ;; mlint-symtab-line-regex
          ;; mlint-version
#+end_src

***** Keys

I try here to be compatible with default =Flycheck= [[file:/emacs.cat/org-config.cat/csb-spell][keys]].

#+begin_src emacs-lisp
  (stante-after mlint
                (define-key mlint-minor-mode-map (kbd "C-c SPC b") 'mlint-buffer)
                (define-key mlint-minor-mode-map (kbd "C-c SPC l") 'mlint-emacs-popup-kludge)
                (define-key mlint-minor-mode-map (kbd "C-c SPC o") 'mlint-mark-ok)
                (define-key mlint-minor-mode-map (kbd "C-c SPC m") 'mlint-minor-menu)
                ;; Next
                (define-key mlint-minor-mode-map (kbd "C-c SPC n") 'mlint-next-buffer)
                (define-key mlint-minor-mode-map (kbd "C-c SPC m") 'mlint-next-buffer-new)
                ;; Previous
                (define-key mlint-minor-mode-map (kbd "C-c SPC p") 'mlint-prev-buffer)
                (define-key mlint-minor-mode-map (kbd "C-c SPC ,") 'mlint-prev-buffer-new)
                ;; Warnings
                (define-key mlint-minor-mode-map (kbd "C-c SPC s") 'mlint-show-warning)
                (define-key mlint-minor-mode-map (kbd "C-c SPC c") 'mlint-clear-warnings)
                (define-key mlint-minor-mode-map (kbd "C-c SPC f") 'mlint-fix-warning))
#+end_src

#+begin_src emacs-lisp
  (stante-after which-key
                (add-to-list 'which-key-allow-regexps "C-c SPC"))
#+end_src

**** Toggle

Toggle flycheck and mlint

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (setq-local csb/toggle-syntax-checker
                          '(progn
                             (call-interactively 'flycheck-mode)
                             (matlab-toggle-show-mlint-warnings)))))
#+end_src

*** Navigate

**** Outshine

#+begin_src emacs-lisp
  (add-hook 'matlab-mode-hook
            (lambda ()
              (outline-minor-mode 1)
              (setq-local outline-regexp "% \\(\\*+\\s-\\)")))
#+end_src

#+begin_src emacs-lisp
  (define-key matlab-mode-map (kbd "C-x c u")
    (lambda (&optional arg) (interactive "P")
      (let ((helm-split-window-default-side
             (if (> (window-width) 140)
                 'right
               'below )))
        (if (equal arg '(4))
            (helm-navi)
          (helm-navi-headings)))))
#+end_src

*** Tools

**** Export org to matlab

Following the literate programming paradigm requires tangling the code to a =matlab=
file. Problem is code is misaligned, etc. Adding this, auto beautifies the
resulting =matlab= file upon tangling.

#+begin_src emacs-lisp
  (add-hook 'org-babel-post-tangle-hook
            (lambda ()
              (when (string= major-mode "matlab-mode")
                (indent-region (point-min) (point-max))
                (save-buffer))))
#+end_src

** Matlab Shell

*** Shell toggle

#+begin_src emacs-lisp
  (add-hook 'matlab-shell-mode-hook
            (lambda ()
              (setq-local csb/toggle-inferior-shell
                          #'csb/term-toggle-MATLAB)))
#+end_src

*** Variables

Be aware that matlab-shell does a (kill-all-local-variables).

#+begin_src emacs-lisp
  (setq matlab-shell-command-switches
        (if running-os-is-linux
            '("-nodesktop -nosplash -nosoftwareopengl")
          '("-nodesktop"))
        matlab-shell-command
        (if running-os-is-linux
            "/opt/MATLAB/R2018a/bin/matlab"
          "c:/MATLAB_R2018a/bin/matlab.exe"))
#+end_src

*** Keys

#+begin_src emacs-lisp
  (add-hook 'matlab-shell-mode-hook
            (lambda ()
              (define-key matlab-shell-mode-map (kbd "C-j") #'comint-send-input)
              (define-key matlab-shell-mode-map
                [remap comint-previous-input] #'matlab-shell-previous-matching-input-from-input)
              (define-key matlab-shell-mode-map
                [remap comint-next-input] #'matlab-shell-next-matching-input-from-input)))
#+end_src

*** Hooks

#+begin_src emacs-lisp
  (add-hook 'matlab-shell-mode-hook
            (lambda ()
              (csb/debug-mode 1)
              (setq-local company-minimum-prefix-length 2)
              (setq show-trailing-whitespace nil)
              (smartscan-mode -1)
              (add-to-list (make-local-variable 'company-backends)
                           'company-matlab-shell)))
#+end_src

** Debugging

It is possible to diagnose problems programmatically. This approach allows to
set breakpoints to pause the execution of programs to examine values where you
think a problem could be. See [[http://fr.mathworks.com/help/matlab/debugging-code.html][this]].

So basically, to debug a file where an error occurs.

 - First, use the 'dbstop' interface documented @ mathworks [[http://www.mathworks.es/es/help/matlab/ref/dbstop.html][site]] by issuing

  dbstop if error
  dbstop in file
  dbstop in file at location
  dbstop in file if expression
  dbstop in file at location if expression
  dbstop if condition
  dbstop(s)
  dbcont
  dbquit
  dbstep
  etc.

 - Secondly, use the gud major mode for interacting with an inferior debugger process, as explained in
the info [[info:emacs#Debuggers][page]]

Eval once after the mode is loaded in opposition to executing the code every time a matlab buffer
is open (hook approach)

The arrow won't move if dbstep from the source code buffer.

** Old Code

Older code

#+begin_src emacs-lisp :tangle no
              ;; (setq-local orgstruct-heading-prefix-regexp " *%% ")
              ;; IMenu
              ;; (add-to-list 'imenu-generic-expression '(nil  "^%% *\\(.*\\)" 1))
              ;; matlab-imenu-generic-expression is defined in matlab.el
              ;; (add-to-list 'imenu-generic-expression '(nil "^ *%% +\\([*]+ .*\\)" 0))
              ;; (add-to-list 'imenu-generic-expression '(nil "^ *%% *\\(\\*.*\\)" 1))
              ;; (add-to-list 'imenu-generic-expression '("Class" "^\\s-*classdef\\>[ \t\n.]*\\(\\(\\[[^]]*\\]\\|\\sw+\\)[ \t\n.]*\< =\[ \t\n.]*\\)?\\([a-zA-Z0-9_]+\\)" 3))
              ;; (eval-after-load 'matlab '(diminish 'mlint-minor-mode))
              ;; (ecb-activate)
              ;; imenu-default-create-index-function is better than gtags
              ;; (setq-local imenu-create-index-function #'ggtags-build-imenu-index)
#+end_src

;;  C-c C-c c - Insert the next CASE condition in a SWITCH.
;;  C-c C-c e - Insert a matched END statement.  With optional ARG, reindent.
;;  C-c C-c ' - Convert plaintext in region to a string with correctly quoted chars.
;;
;; Convenient editing commands are:
;;  C-c ;   - Comment/Uncomment out a region of code.
;;  C-c C-f - Fill the current comment line.
;;  C-c C-q - Fill code and comments in region.
;;  M-q     - Refill the current command or comment.
;;  C-M-i   - Symbol completion of matlab symbolsbased on the local syntax.
;;  C-M-q - Indent syntactic block of code.
;;
;; Convenient navigation commands are:
;;  M-a   - Move to the beginning of a command.
;;  M-e   - Move to the end of a command.
;;  C-M-a - Move to the beginning of a function.
;;  C-M-e - Move do the end of a function.
;;  C-M-f - Move forward over a syntactic block of code.
;;  C-M-b - Move backwards over a syntactic block of code.
;;
;;
;; All Key Bindings:
;; key             binding
;; ---             -------
;;
;; C-c             Prefix Command
;; C-h             Prefix Command
;; C-j             matlab-linefeed
;; ESC             Prefix Command
;; %               matlab-electric-comment
;; <C-M-mouse-2>   matlab-find-file-click
;; <C-M-return>    matlab-shell-run-cell
;; <return>        matlab-return
;;

;; C-M-i           matlab-complete-symbol
;; M-RET           newline

;; M-a             matlab-beginning-of-command
;; M-e             matlab-end-of-command
;; M-j             matlab-comment-line-break-function
;; M-q             matlab-fill-paragraph
;; M-s             matlab-show-matlab-shell-buffer
;;
;; C-h RET         Prefix Command
;;
;; C-c C-c         Prefix Command
;; C-c C-f         matlab-fill-comment-line
;; C-c C-j         matlab-justify-line

;; C-c C-t         matlab-show-line-info
;; C-c <return>    matlab-comment-return

;; (global-unset-key (kbd "C-M-Return") )
