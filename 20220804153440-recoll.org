:PROPERTIES:
:ID:       62241218-e145-4123-9b66-2b99314d6fe3
:END:
#+title: recoll

[[file:/software.cat/cli.cat/recoll::*Introduction][Recoll]] is a desktop indexer. Its page is [[file:/software.cat/cli.cat/recoll::*Introduction][here]].

* Systemd units

** Service
:PROPERTIES:
:header-args: :tangle (format "%s/%s" (getenv "HOME") ".config/systemd/user/recoll@.service") :mkdirp yes :tangle-mode (identity #o444)
:END:

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################

  [Unit]
  Description=Index data
  ConditionACPower=true

  [Service]
  Environment=RCLCRON_RCLINDEX=
  Environment=RECOLL_CONFDIR=/home/csantos/.recoll/%i/
  Type=simple
  ExecStart=/home/csantos/Scripts/systemd-recoll.sh %i
  WorkingDirectory=/home/csantos/.recoll/%i/
#+end_src

Using [[file:/scrips.cat/scripts::*Systemd-recoll%20ancillary][this]] script as =ExecStart=.

** Timer
:PROPERTIES:
:header-args: :tangle (format "%s/%s" (getenv "HOME") ".config/systemd/user/recoll@.timer") :mkdirp yes :tangle-mode (identity #o444)
:END:

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################

  [Unit]
  Description=Index data timer
  Documentation=http://duplicity.nongnu.org/

  [Timer]
  # Time to wait after booting before we run first time
  # OnBootSec=15min
  # Time between running each consecutive time
  # OnUnitActiveSec=1d
  OnCalendar=daily
  # OnCalendar=Mon-Fri  *-*-* 18:00:00
  # AccuracySec=1h
  # start once immediately if it missed the last start time
  Persistent=true
  Unit=recoll@%i.service

  [Install]
  WantedBy=default.target
#+end_src
** Use

The following units allow running a daily job to index several
different sources. To enable/start/stop them use

#+begin_src sh :tangle no
  userctl start recoll@wikidata-perso.timer
  userctl start recoll@wikidata-apc.timer
  userctl start recoll@nextcloud.timer
  userctl start recoll@home.timer
#+end_src
