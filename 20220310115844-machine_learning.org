:PROPERTIES:
:ID:       a08701b3-a963-424b-975d-26837f44b4d9
:END:
#+title: machine learning
#+filetags: :ml:machine-learning:ai:

* Intro

[[https://www.ionos.fr/digitalguide/web-marketing/analyse-web/quest-ce-que-lapprentissage-automatique/][Comment fonctionne le machine learning ?]]

* Types

 - l’apprentissage supervisé

   - l’apprentissage semi-supervisé

   - l’apprentissage par [[https://www.techtarget.com/searchenterpriseai/definition/reinforcement-learning][renforcement]] (reinforcement learning)

   - l’apprentissage actif

 - l’apprentissage non supervisé

Then

 - shallow learning

 - [[id:20b8e7d6-29a4-4106-bd86-9447fe6d917b][Deep learning]]

* Tips

*Feature selection* is the process of subsetting the data to a more relevant and informative set.

*Data munging* is transforming the data into a format more suitable for modeling.

[[id:71ee5c74-d3bf-42e3-bea9-bffd015331d3][Principal Component Analysis]] is an unsupervised, non-parametric statistical [[https://medium.com/apprentice-journal/pca-application-in-machine-learning-4827c07a61db][technique]]
primarily used for dimensionality reduction in machine learning.

[[https://docs.microsoft.com/en-us/azure/machine-learning/concept-fairness-ml#what-is-machine-learning-fairness][Fairness]]
https://fairlearn.org/

*Clustering* and *classification* :: Classifications take a set of data that you’ve already manually
analyzed and labeled and uses that to train a learning model to then examine a set of new data. This
is called supervised learning. Clustering on the other hand, doesn’t require an existing data set
that’s been labeled by humans but still tries to find the groupings and differences in the data.
This is called unsupervised learning.

Classification examples are Logistic regression, Naive Bayes classifier, Support vector machines,
etc. Whereas clustering examples are k-means clustering algorithm, Fuzzy c-means clustering
algorithm, Gaussian (EM) clustering algorithm, etc.

See for example: https://www.geeksforgeeks.org/ml-classification-vs-clustering/

* References

- [[id:4ba81158-8744-40d4-b568-de596b480f74][fast_ml_lab]]

- https://machine-learning.pages.in2p3.fr/

https://medium.com/apprentice-journal/pca-application-in-machine-learning-4827c07a61db

https://cpb-us-e2.wpmucdn.com/faculty.sites.uci.edu/dist/2/51/files/2018/05/JPM-2017-MachineLearningInvestments.pdf

https://martin.zinkevich.org/rules_of_ml/rules_of_ml.pdf

 - [[https://twitter.com/nevrekaraishwa2/status/1568873943477657600?t=IEd6JJg9hVh5FDEp5wfgNw&s=09][Machine Learning Interview Questions resources.]]

 - [[telega:-1001166929514?open_content#728][◀Personal🕺: 📎 ai-E7T0Ie.org]]

 - https://www.scientificamerican.com/article/how-a-machine-learns-prejudice/

 - https://datascientest.com/blog-data-ia-actualites/machine-learning

 - [[file:~/Projects/l2it/og/wiki.wiki/uploads/c7ac53341a4ff711391a2f9d1b5d5702/Présentation2.pdf::%PDF-1.3][Sylvain Caillou (1/3)]]

** Books

- [[https://www.cs.huji.ac.il/~shais/UnderstandingMachineLearning/copy.html][Understanding Machine Learning: From Theory to Algorithms]]

- [[https://www.databricks.com/p/ebook/big-book-of-machine-learning-use-cases?utm_medium=paid+social&utm_source=linkedin&utm_campaign=618606756&utm_adgroup=173650206&utm_content=ebook&utm_offer=big-book-of-machine-learning-use-cases&utm_ad=155403326][The Big Book of Machine Learning Use Cases]]

- [[https://www.amazon.com/Python-Deep-Learning-techniques-architectures-dp-1789348463/dp/1789348463?_encoding=UTF8&me=&qid=1653429318&linkCode=sl1&tag=kirkdborne-20&linkId=1f44901398c15b391a3872517d82623c&language=en_US&ref_=as_li_ss_tl][Python Deep Learning:]], [[http://alvarestech.com/temp/deep/Python%20Deep%20Learning%20Exploring%20deep%20learning%20techniques,%20neural%20network%20architectures%20and%20GANs%20with%20PyTorch,%20Keras%20and%20TensorFlow%20by%20Ivan%20Vasilev,%20Daniel%20Slater,%20Gianmario%20Spacagna,%20Peter%20Roelants,%20Va%20(z-lib.org).pdf][book.pdf]]

** Training

https://linuxhint.com/scikit-learn-tutorial/

 - [[https://www.mltut.com/best-math-courses-for-machine-learning/][12 Best Math Courses for Machine Learning and Data Science in 2022]]

 - [[id:e392425f-5f52-41fe-b02d-191a1d6086fa][escape2022]] summer school

 - [[https://www.youtube.com/playlist?list=PLoROMvodv4rMiGQp3WXShtMGgzqpfVfbU][Stanford CS229: Machine Learning Full Course taught by Andrew Ng | Autumn 2018]]

 - Mooco: machine learning in Python with scikit-learn

   + [[telega:-1001166929514?open_content#728][Telegram AI org file]]
   + [[telega:-1001166929514#728][Telegram AI entry]]

 - https://github.com/JuliaAcademy/Foundations-of-Machine-Learning

 - https://github.com/tudo-astroparticlephysics/machine-learning-lecture


 _Fun Mooc - Machine Learning_

  - https://www.fun-mooc.fr/en/courses/machine-learning-python-scikit-learn/

  - Code repository: https://github.com/INRIA/scikit-learn-mooc/

  - The static version of the course can be browsed online: https://inria.github.io/scikit-learn-mooc

  - All course videos: https://www.youtube.com/playlist?list=PL2okA_2qDJ-m44KooOI7x8tu85wr4ez4f
