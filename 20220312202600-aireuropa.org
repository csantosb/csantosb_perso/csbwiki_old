:PROPERTIES:
:ID:       b889c696-a7b0-45fb-bd7e-41a62b8eb56d
:END:
#+title: aireuropa

* Bagages

https://www.aireuropa.com/fr/fr/aea/informations-pour-voler/bagages/bagages-a-main.html

20 x 35 x 30 cm.

55 cm x 35 cm x 25 cm (ou la somme des trois dimensions ne doit pas dépasser 115 cm linéaires)
