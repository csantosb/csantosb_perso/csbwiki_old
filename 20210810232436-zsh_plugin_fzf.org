:PROPERTIES:
:ID:       dc6da772-3964-4d84-b614-38887944054c
:header-args:    :tangle /home/csantos/DotFiles/oh-my-zsh/custom/plugins/fzf/fzf.plugin.zsh :mkdirp yes :tangle-mode (identity #o444)
:END:
#+title: zsh-plugin-fzf
#+filetags: zsh fzf plugin

The original plugins are installed in =/etc/profile.d/fzf.zsh= and
=/etc/profile.d/fzf-extras.zsh=, auto [[https://wiki.archlinux.org/index.php/Autostarting#.2Fetc.2Fprofile][sourced]].

[[id:c11d8622-faa4-4486-9d30-c8a91df2b32d][fzf]].

[[file:/usr/share/doc/fzf-extras]]

#+begin_src sh :padline no
  #######################################################################
  #  DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING PAGE !!! #
  #######################################################################
#+end_src

* pass completion suggested by @d4ndo (#362)

#+begin_src sh :tangle no
  _fzf_complete_pass() {
      _fzf_complete '+m' "$@" < <(
          pwdir=${PASSWORD_STORE_DIR-~/.password-store/}
          stringsize="${#pwdir}"
          find "$pwdir" -name "*.gpg" -print |
              cut -c "$((stringsize + 1))"-  |
              sed -e 's/\(.*\)\.gpg/\1/'
      )
  }
#+end_src

* File open

fe [FUZZY PATTERN] - Open the selected file with the default editor
Bypass fuzzy finder if there's only one match (--select-1)
Exit if there's no match (--exit-0)

#+begin_src sh :tangle no
  fe() {
      local file
      file=$(fzf --query="$1" --select-1 --exit-0)
      [ -n "$file" ] && ${EDITOR:-vim} "$file"
  }
#+end_src

Modified version where you can press
  - CTRL-O to open with `open` command,
  - CTRL-E or Enter key to open with the $EDITOR

#+begin_src sh :tangle no
  fo() {
      local out file key
      out=$(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e)
      key=$(head -1 <<< "$out")
      file=$(head -2 <<< "$out" | tail -1)
      if [ -n "$file" ]; then
          [ "$key" = ctrl-o ] && open "$file" || ${EDITOR:-vim} "$file"
      fi
  }
#+end_src

* CD

fcd - cd to selected directory

#+begin_src sh :tangle no
  fcd() {
      local dir
      dir=$(find ${1:-*} -path '*/\.*' -prune \
                 -o -type d -print 2> /dev/null | fzf +m) &&
          cd "$dir"
  }
#+end_src

fda - including hidden directories

#+begin_src sh :tangle no
  fcda() {
      local dir
      dir=$(find ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
  }
#+end_src
