:PROPERTIES:
:ID:       c0256c28-ee95-4579-ba26-035bccbfc337
:header-args:    :tangle-mode (identity #o444)
:END:
#+title: proced
#+filetags: emacs sw paradox

* Introduction

Generate a listing of UNIX system processes. See [[help:proced-mode][proced-mode help]] for details.

* Emacs configuration

#+begin_src emacs-lisp
  (use-package proced

    :init
    (defun csb/proced ()
      (interactive)
      ;; (require 'csb-convenience) ; provides tab-bar defuns
      ;; (csb/tab-get-to-or-create "Proced")
      (if (not (get-buffer "*Proced*"))
          (proced))
      (switch-to-buffer (get-buffer "*Proced*")))

    :commands (proced)

    :bind
    (:map proced-mode-map
          (("i" . proced-narrow))
          :map launcher-map
          ("P" . csb/proced))

    :hook
    (proced-mode . (lambda ()
                     (proced-toggle-auto-update t)))

    :custom

    (proced-auto-update-interval 2)

    ;; [[https://github.com/travisjeffery/proced-narrow][Proced narrow]]
    (use-package proced-narrow
      :after proced
      :commands proced-narrow
      :bind (:map proced-mode-map
                  ("a" . proced-toggle-auto-update)
                  ("i" . (lambda ()
                           (interactive)
                           (proced-toggle-auto-update -1)
                           (proced-narrow))))))
#+end_src

_References_

 - [[http://www.masteringemacs.org/article/displaying-interacting-processes-proced][Displaying and Interacting with processes using Proced]]
 - [[http://emacsredux.com/blog/2013/05/02/manage-processes-with-proced/][Manage Processes With Proced]]
 - [[elfeed:irreal.org#https://irreal.org/blog/?p=10585][Proced]]
